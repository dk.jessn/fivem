[< Tilbage](README.md)

# FiveM Server Installation

## Trin 0 - Bliv medlem af CitizenFX

_Dette trin er frivilligt._

Køb [Argentum medlemskab](https://www.patreon.com/join/fivem) for at bruge custom clothing som for eksempel dine egne uniformer.

## Trin 1 - Installer database

Download og installer MySQL Community Edition.

_Husk koden til databasen!_

```
https://dev.mysql.com/downloads/mysql/
```

## Trin 2 - Hent Modding Framework

Download modding framework (det her repository) til din maskine enten med Git som vist nedenfor eller ved brug af **Download-knappen** på [GitLab](https://gitlab.com/dk.jessn/fivem).

_Det anbefales at bruge FlaxHosting filerne._

```
git clone https://gitlab.com/dk.jessn/fivem.git
```

## Trin 3 - Installer FiveM Server

Download den _seneste_ eller anbefalede version og installer/udpak FiveM Server binaries til kataloget `server`.

```
https://runtime.fivem.net/artifacts/fivem/build_server_windows/master/
```

## Trin 4 - Opret database

Forbind til databasen med MySQL Workbench og opret databasen med denne kommando `CREATE SCHEMA fivem DEFAULT CHARACTER SET utf8 COLLATE utf8_danish_ci;`.

### Database kun til ESX

Opret herefter alle tabellerne med scriptet **database.sql**, der ligger i kataloget `txDataEsx\ESXLegacy_010DCE.base`.

### Database kun til DEVO

Opret herefter alle tabellerne med scriptet **database.sql**, der ligger i kataloget `txDataVrpDevo\CFXDefault_Devo.base`.

## Trin 5 - Konfigurer serveren

Opret filen **configurations.cfg** med nedenstående indhold i et af følgende kataloger, afhængig af hvilken server du vil bruge, og ret som minimúm `sv_licenseKey`, `sv_hostname`, `sv_projectName`, `sv_projectDesc`, `mysql_connection_string` i filens indhold.  

- For Dunko VRP <br>
txDataVrpDunko\CFXDefault_E9235B.base
- For ESX <br>
txDataEsx\ESXLegacy_010DCE.base
- For DEVO <br>
txDataVrpDevo\CFXDefault_Devo.base

```configurations.cfg
set mysql_connection_string "mysql://USERNAME:PASSWORD@localhost/fivem?multipleStatements=true"
sets Discord ""
sets YouTube ""
sets tags ""

sv_hostname ""
sets sv_projectName ""
sets sv_projectDesc ""

sv_licenseKey ""
set steam_webApiKey ""

sv_maxclients 32
```

### Konfiguratonsfil kun til VRP Dunko og DEVO

Opret filen **config.json** i `resources\ghmattimysql` med nedenstående indhold 
og ret som minimum `user`, `password` og `database` i filens indhold.  

```config.json
{
    "user": "",
    "password": "",
    "host": "localhost",
    "port": 3306,
    "database": "fivem"
}
```

## Trin 6 - Optart af serveren 

Start serveren  fra et af følgende kataloger, afhængig af hvilken server du vil bruge, med nedenstående kommando.

- For Dunko VRP  <br>
txDataVrpDunko\CFXDefault_E9235B.base
- For ESX <br>
txDataEsx\ESXLegacy_010DCE.base
- For DEVO <br>
txDataVrpDevo\CFXDefault_Devo.base

```
start ../../server/FXServer
```

Opret eventuelt en fil **start.bat** med nedenstående indhold i kataloget.

```start.bat
@echo off
start ../../server/FXServer
```

Start herefter serveren ved at køre filen.

## Trin 7 - Ændring af navn på serveren

_Dette trin er frivilligt._

Ændre navne og billeder i nedenstående filer for at skifte navn på serveren.

- txDataVrpFlax\CFXDefault_Flax.base/logo.png
- txDataVrpFlax\CFXDefault_Flax.base/server.cfg
- txDataVrpFlax\CFXDefault_Flax.base/server_global_settings.cfg
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/gcphone/html/static/config/config.json
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/gcphone/html/static/img/background
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/dn-disclaimer/ui/img/bg-img.png
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/dn-info/img/header.png
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/dn-loadingscreen/Loading.mp3
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/dn-loadingscreen/Loading.ogg
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/dn-loadingscreen/loading.png
- txDataVrpFlax\CFXDefault_Flax.base/resources/[vrp]/vrp/modules/gui.lua `line 151`
- txDataVrpFlax\CFXDefault_Flax.base/resources/[vrp]/vrp/gui/design.css `line 173`
- txDataVrpFlax\CFXDefault_Flax.base/resources/[Misc]/logo/ui/img/logo.png
- txDataVrpFlax\CFXDefault_Flax.base/resources/[vrp]/vrp/cfg/lang/da.lua `line 6`
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/dn-ui/bare.lua `line 6`
