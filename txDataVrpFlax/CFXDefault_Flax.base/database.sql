-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: fivem.tornebakken11-45.dk    Database: devo
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `crypto`
--

DROP TABLE IF EXISTS `crypto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crypto` (
  `crypto` varchar(50) NOT NULL DEFAULT 'qbit',
  `worth` int NOT NULL DEFAULT '0',
  `history` text,
  PRIMARY KEY (`crypto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crypto_transactions`
--

DROP TABLE IF EXISTS `crypto_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `crypto_transactions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `citizenid` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `message` varchar(50) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `citizenid` (`citizenid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lapraces`
--

DROP TABLE IF EXISTS `lapraces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lapraces` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `checkpoints` text,
  `records` text,
  `creator` varchar(50) DEFAULT NULL,
  `distance` int DEFAULT NULL,
  `raceid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `omik_polititabletan`
--

DROP TABLE IF EXISTS `omik_polititabletan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `omik_polititabletan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `name` text NOT NULL,
  `phone` text NOT NULL,
  `profileLogo` text NOT NULL,
  `rank` text NOT NULL,
  `extraRank` text NOT NULL,
  `badgeNumber` text NOT NULL,
  `password` text NOT NULL,
  `ranks` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `certs` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `omik_polititabletan_chk_1` CHECK (json_valid(`ranks`)),
  CONSTRAINT `omik_polititabletan_chk_2` CHECK (json_valid(`certs`))
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `omik_polititabletans`
--

DROP TABLE IF EXISTS `omik_polititabletans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `omik_polititabletans` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `type` text NOT NULL,
  `badge` int NOT NULL,
  `name` text NOT NULL,
  `reason` text NOT NULL,
  `dato` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `icon` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `omik_polititabletbo`
--

DROP TABLE IF EXISTS `omik_polititabletbo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `omik_polititabletbo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `law` text NOT NULL,
  `para` text NOT NULL,
  `offense` text NOT NULL,
  `price` int NOT NULL,
  `clip` int NOT NULL,
  `jail` int DEFAULT '0',
  `disq` text NOT NULL,
  `info` text NOT NULL,
  `amount` int DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `omik_polititabletef`
--

DROP TABLE IF EXISTS `omik_polititabletef`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `omik_polititabletef` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creator` text NOT NULL,
  `name` text NOT NULL,
  `offense` text NOT NULL,
  `reason` text NOT NULL,
  `price` int NOT NULL,
  `jail` int NOT NULL,
  `disq` text NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `omik_polititabletefk`
--

DROP TABLE IF EXISTS `omik_polititabletefk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `omik_polititabletefk` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creator` text NOT NULL,
  `numberplate` text NOT NULL,
  `reason` text NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `omik_polititabletinf`
--

DROP TABLE IF EXISTS `omik_polititabletinf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `omik_polititabletinf` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `writer` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `omik_polititabletkr`
--

DROP TABLE IF EXISTS `omik_polititabletkr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `omik_polititabletkr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `cprNumber` int NOT NULL,
  `profileLogo` text NOT NULL,
  `height` int NOT NULL,
  `clip` int NOT NULL,
  `phone` text NOT NULL,
  `disq` text NOT NULL,
  `name` text NOT NULL,
  `age` int NOT NULL,
  `bdate` text NOT NULL,
  `records` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `omik_polititabletkr_chk_1` CHECK (json_valid(`records`))
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `phone_gallery`
--

DROP TABLE IF EXISTS `phone_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `phone_gallery` (
  `citizenid` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `phone_invoices`
--

DROP TABLE IF EXISTS `phone_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `phone_invoices` (
  `id` int NOT NULL AUTO_INCREMENT,
  `citizenid` varchar(50) DEFAULT NULL,
  `amount` int NOT NULL DEFAULT '0',
  `society` tinytext,
  `sender` varchar(50) DEFAULT NULL,
  `sendercitizenid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `citizenid` (`citizenid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `phone_messages`
--

DROP TABLE IF EXISTS `phone_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `phone_messages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `citizenid` varchar(50) DEFAULT NULL,
  `number` varchar(50) DEFAULT NULL,
  `messages` text,
  PRIMARY KEY (`id`),
  KEY `citizenid` (`citizenid`),
  KEY `number` (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `phone_tweets`
--

DROP TABLE IF EXISTS `phone_tweets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `phone_tweets` (
  `id` int NOT NULL AUTO_INCREMENT,
  `citizenid` varchar(50) COLLATE utf8mb3_danish_ci DEFAULT NULL,
  `firstName` varchar(25) COLLATE utf8mb3_danish_ci DEFAULT NULL,
  `lastName` varchar(25) COLLATE utf8mb3_danish_ci DEFAULT NULL,
  `message` text COLLATE utf8mb3_danish_ci,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `url` text COLLATE utf8mb3_danish_ci,
  `picture` text COLLATE utf8mb3_danish_ci,
  `tweetId` varchar(25) COLLATE utf8mb3_danish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `citizenid` (`citizenid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `player_contacts`
--

DROP TABLE IF EXISTS `player_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `player_contacts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `citizenid` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `number` varchar(50) DEFAULT NULL,
  `iban` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `citizenid` (`citizenid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `player_mails`
--

DROP TABLE IF EXISTS `player_mails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `player_mails` (
  `id` int NOT NULL AUTO_INCREMENT,
  `citizenid` varchar(50) DEFAULT NULL,
  `sender` varchar(50) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `message` text,
  `read` tinyint DEFAULT '0',
  `mailid` int DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `button` text,
  PRIMARY KEY (`id`),
  KEY `citizenid` (`citizenid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `player_vehicles`
--

DROP TABLE IF EXISTS `player_vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `player_vehicles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `license` varchar(50) COLLATE utf8mb3_danish_ci DEFAULT NULL,
  `citizenid` varchar(50) COLLATE utf8mb3_danish_ci DEFAULT NULL,
  `vehicle` varchar(50) COLLATE utf8mb3_danish_ci DEFAULT NULL,
  `hash` varchar(50) COLLATE utf8mb3_danish_ci DEFAULT NULL,
  `mods` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `plate` varchar(50) COLLATE utf8mb3_danish_ci NOT NULL,
  `fakeplate` varchar(50) COLLATE utf8mb3_danish_ci DEFAULT NULL,
  `garage` varchar(50) COLLATE utf8mb3_danish_ci DEFAULT NULL,
  `fuel` int DEFAULT '100',
  `engine` float DEFAULT '1000',
  `body` float DEFAULT '1000',
  `state` int DEFAULT '1',
  `depotprice` int NOT NULL DEFAULT '0',
  `drivingdistance` int DEFAULT NULL,
  `status` text COLLATE utf8mb3_danish_ci,
  `balance` int NOT NULL DEFAULT '0',
  `paymentamount` int NOT NULL DEFAULT '0',
  `paymentsleft` int NOT NULL DEFAULT '0',
  `financetime` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `plate` (`plate`),
  KEY `citizenid` (`citizenid`),
  KEY `license` (`license`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vrp_srv_data`
--

DROP TABLE IF EXISTS `vrp_srv_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrp_srv_data` (
  `dkey` varchar(255) NOT NULL,
  `dvalue` text,
  PRIMARY KEY (`dkey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vrp_user_business`
--

DROP TABLE IF EXISTS `vrp_user_business`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrp_user_business` (
  `user_id` int NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `description` text,
  `capital` int DEFAULT NULL,
  `laundered` int DEFAULT NULL,
  `reset_timestamp` int DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_business_users` FOREIGN KEY (`user_id`) REFERENCES `vrp_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vrp_user_data`
--

DROP TABLE IF EXISTS `vrp_user_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrp_user_data` (
  `user_id` int NOT NULL,
  `dkey` varchar(255) NOT NULL,
  `dvalue` text,
  PRIMARY KEY (`user_id`,`dkey`),
  CONSTRAINT `fk_user_data_users` FOREIGN KEY (`user_id`) REFERENCES `vrp_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vrp_user_homes`
--

DROP TABLE IF EXISTS `vrp_user_homes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrp_user_homes` (
  `user_id` int NOT NULL,
  `home` varchar(255) DEFAULT NULL,
  `number` int DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_homes_users` FOREIGN KEY (`user_id`) REFERENCES `vrp_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vrp_user_identities`
--

DROP TABLE IF EXISTS `vrp_user_identities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrp_user_identities` (
  `user_id` int NOT NULL,
  `registration` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `age` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vrp_user_ids`
--

DROP TABLE IF EXISTS `vrp_user_ids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrp_user_ids` (
  `identifier` varchar(255) NOT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`identifier`),
  KEY `fk_user_ids_users` (`user_id`),
  CONSTRAINT `fk_user_ids_users` FOREIGN KEY (`user_id`) REFERENCES `vrp_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vrp_user_moneys`
--

DROP TABLE IF EXISTS `vrp_user_moneys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrp_user_moneys` (
  `user_id` int NOT NULL,
  `wallet` int DEFAULT NULL,
  `bank` int DEFAULT NULL,
  `debt` int DEFAULT '0',
  `depositOnLogin` int DEFAULT '0',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_moneys_users` FOREIGN KEY (`user_id`) REFERENCES `vrp_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vrp_user_tokens`
--

DROP TABLE IF EXISTS `vrp_user_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrp_user_tokens` (
  `token` varchar(200) NOT NULL,
  `user_id` int DEFAULT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vrp_user_vehicles`
--

DROP TABLE IF EXISTS `vrp_user_vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrp_user_vehicles` (
  `user_id` int NOT NULL,
  `vehicle` varchar(100) NOT NULL,
  `veh_type` varchar(255) NOT NULL DEFAULT 'default',
  `vehicle_plate` varchar(255) NOT NULL,
  `vehicle_colorprimary` varchar(255) DEFAULT '0',
  `vehicle_colorsecondary` varchar(255) DEFAULT '0',
  `vehicle_pearlescentcolor` varchar(255) DEFAULT '0',
  `vehicle_wheelcolor` varchar(255) DEFAULT '0',
  `vehicle_plateindex` varchar(255) DEFAULT '0',
  `vehicle_neoncolor1` varchar(255) DEFAULT '0',
  `vehicle_neoncolor2` varchar(255) DEFAULT '0',
  `vehicle_neoncolor3` varchar(255) DEFAULT '0',
  `vehicle_windowtint` varchar(255) DEFAULT '0',
  `vehicle_wheeltype` varchar(255) DEFAULT '0',
  `vehicle_mods0` varchar(255) DEFAULT '-1',
  `vehicle_mods1` varchar(255) DEFAULT '-1',
  `vehicle_mods2` varchar(255) DEFAULT '-1',
  `vehicle_mods3` varchar(255) DEFAULT '-1',
  `vehicle_mods4` varchar(255) DEFAULT '-1',
  `vehicle_mods5` varchar(255) DEFAULT '-1',
  `vehicle_mods6` varchar(255) DEFAULT '-1',
  `vehicle_mods7` varchar(255) DEFAULT '-1',
  `vehicle_mods8` varchar(255) DEFAULT '-1',
  `vehicle_mods9` varchar(255) DEFAULT '-1',
  `vehicle_mods10` varchar(255) DEFAULT '-1',
  `vehicle_mods11` varchar(255) DEFAULT '-1',
  `vehicle_mods12` varchar(255) DEFAULT '-1',
  `vehicle_mods13` varchar(255) DEFAULT '-1',
  `vehicle_mods14` varchar(255) DEFAULT '-1',
  `vehicle_mods15` varchar(255) DEFAULT '-1',
  `vehicle_mods16` varchar(255) DEFAULT '-1',
  `vehicle_turbo` varchar(255) NOT NULL DEFAULT 'off',
  `vehicle_tiresmoke` varchar(255) NOT NULL DEFAULT 'off',
  `vehicle_xenon` varchar(255) NOT NULL DEFAULT 'off',
  `vehicle_mods23` varchar(255) DEFAULT '-1',
  `vehicle_mods24` varchar(255) DEFAULT '-1',
  `vehicle_neon0` varchar(255) DEFAULT 'off',
  `vehicle_neon1` varchar(255) DEFAULT 'off',
  `vehicle_neon2` varchar(255) DEFAULT 'off',
  `vehicle_neon3` varchar(255) DEFAULT 'off',
  `vehicle_bulletproof` varchar(255) DEFAULT '-1',
  `vehicle_smokecolor1` varchar(255) DEFAULT '255',
  `vehicle_smokecolor2` varchar(255) DEFAULT '255',
  `vehicle_smokecolor3` varchar(255) DEFAULT '255',
  `vehicle_modvariation` varchar(255) NOT NULL DEFAULT 'off',
  `vehicle_fuel` varchar(50) DEFAULT '60.0',
  `vehicle_damage` varchar(50) DEFAULT '1000.0',
  `vehicle_impound` int DEFAULT '0',
  `vehicle_impound_reason` varchar(200) DEFAULT NULL,
  `rented` int DEFAULT '0',
  `hashkey` varchar(255) DEFAULT NULL,
  `impound` int DEFAULT '0',
  `modifications` mediumtext,
  PRIMARY KEY (`user_id`,`vehicle`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vrp_users`
--

DROP TABLE IF EXISTS `vrp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrp_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `last_login` varchar(255) DEFAULT NULL,
  `last_date` varchar(255) NOT NULL DEFAULT '',
  `whitelisted` tinyint(1) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT NULL,
  `DmvTest` int NOT NULL DEFAULT '0',
  `ban_reason` varchar(250) DEFAULT NULL,
  `discord` varchar(250) DEFAULT NULL,
  `warnings` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=801 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vrp_wanted`
--

DROP TABLE IF EXISTS `vrp_wanted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrp_wanted` (
  `user_id` int DEFAULT NULL,
  `wantedreason` varchar(100) DEFAULT NULL,
  `wantedby` int DEFAULT NULL,
  `timestamp` int DEFAULT NULL,
  `count` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vrp_warnings`
--

DROP TABLE IF EXISTS `vrp_warnings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrp_warnings` (
  `warning_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `warning_type` varchar(25) DEFAULT NULL,
  `duration` int DEFAULT NULL,
  `admin` varchar(100) DEFAULT NULL,
  `warning_date` date DEFAULT NULL,
  `reason` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`warning_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'devo'
--
/*!50003 DROP FUNCTION IF EXISTS `PASSWORD` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `PASSWORD`(s VARCHAR(256)) RETURNS varchar(256) CHARSET utf8mb3 COLLATE utf8mb3_danish_ci
    DETERMINISTIC
return CONCAT('*', UPPER(SHA1(UNHEX(SHA1('mypass'))))) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-08-10 16:03:35
