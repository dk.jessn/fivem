Config = {}; // Don't touch

Config.ServerIP = "localhost:30120";

// Social media buttons on the left side
Config.Socials = [
    {name: "discord", label: "Discord", description: "Her kan du finde vores discord", icon: "assets/media/icons/discord.png", link: "https://discord.gg/96MnNNUFHn"},
    {name: "", label: "Youtube", description: "Her kan du finde vores youtube kanal", icon: "assets/media/icons/youtube.png", link: "https://youtube.com/channel/UCJ36Rnj20TsYHbOMo9n5cAg"},
];

Config.HideoverlayKeybind = 112 // JS key code https://keycode.info
Config.CustomBindText = "F1"; // leave as "" if you don't want the bind text in html to be statically set

// Staff list
Config.Staff = [
    {name: "marcusdk4697", description: "Ejer/Udvikler", color: "#fff", image: "https://ih1.redbubble.net/image.467902814.2780/st,small,845x845-pad,1000x1000,f8f8f8.u1.jpg"},
    {name: "Tacodk", description: "Medejer", color: "#fff", image: "https://media.istockphoto.com/id/1356776254/vector/taco-icon-vector.jpg?s=612x612&w=0&k=20&c=iMFWSET_Wu8rM1MW9W1_FVNykw4HZE9KVXWioH7U694="},
    {name: "IsA", description: "Medejer", color: "#fff", image: "https://t3.ftcdn.net/jpg/03/75/67/78/240_F_375677878_T4fjvxu4FjWxKgeDkuscoxn1bYFgPuxi.jpg"},
    {name: "dk.jessn", description: "Udvikler", color: "#fff", image: "https://news.sophos.com/wp-content/uploads/2016/11/root-1200-1.png"},	
	{name: "mikkelj.h.", description: "Bestyrelse", color: "#fff", image: "https://i.pinimg.com/736x/ab/38/03/ab3803dcda0515613f9bba8b4f5b66f1.jpg"},		
]

// Categories
Config.Categories = [
    {label: "Social Media", default: true},
    {label: "Staff", default: false}
];

// Music
Config.Song = "song.mp3";