fx_version "adamant"
game "gta5"
this_is_a_map 'yes'


author 'Fazzl'
description 'Madrazzo Ranch Stage'
version '1.0.0'


data_file "DLC_ITYP_REQUEST" "stream/kg_prop_stagespeaker01_model_details.ytyp"
data_file "DLC_ITYP_REQUEST" "stream/lafa2k_tutorial.ytyp" 
data_file "DLC_ITYP_REQUEST" "stream/kg_prop_smallstage_model_details.ytyp"
data_file "DLC_ITYP_REQUEST" "stream/kg_prop_cdj3_model_details.ytyp"
