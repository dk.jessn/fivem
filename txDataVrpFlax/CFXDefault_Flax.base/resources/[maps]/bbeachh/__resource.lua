resource_manifest_version "44febabe-d386-4d18-afbe-5e627f4af937"
this_is_a_map 'yes'

files {
	'drfl_soundgame.dat151.rel',
	'drfl_sounds.dat54.rel',
	'audio/dlc_radioncs/clear_my_head.awc',
	'audio/dlc_radioncs/drakkar.awc',
	'audio/dlc_radioncs/feel_like_horrible.awc',
	'audio/dlc_radioncs/hold_on.awc',
	'audio/dlc_radioncs/losing_control.awc',
	'audio/dlc_radioncs/overdosing.awc',
	'audio/dlc_radioncs/stardust.awc',
}

data_file 'AUDIO_WAVEPACK' 'audio/dlc_radioncs'
data_file 'AUDIO_GAMEDATA'	'drfl_soundgame.dat'
data_file 'AUDIO_SOUNDDATA'	'drfl_sounds.dat'