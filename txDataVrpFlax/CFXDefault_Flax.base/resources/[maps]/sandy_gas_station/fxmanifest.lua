fx_version 'cerulean'
game 'gta5'

this_is_a_map 'yes'

data_file 'AUDIO_GAMEDATA' 'audio/sandys_gas_col_game.dat'
data_file 'AUDIO_DYNAMIXDATA' 'audio/sandys_gas_col_mix.dat'

files {
	'audio/sandys_gas_col_game.dat151.rel',
	'audio/sandys_gas_col_mix.dat15.rel',
}