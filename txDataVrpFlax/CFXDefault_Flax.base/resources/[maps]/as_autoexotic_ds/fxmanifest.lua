fx_version 'cerulean'
games { 'gta5' }


version '1.3'

this_is_a_map 'yes'

escrow_ignore {
    'stream/**/*.ytd',
    'stream/**/*.ymap'
}