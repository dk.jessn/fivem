local function OnPlayerConnecting(name, setKickReason, deferrals)
    local player = source
    local steamIdentifier
    local allowed = false
    local identifiers = GetPlayerIdentifiers(player)
    local printIds = os.date("%c") .. " "
    local hasDiscord = false
    local hasSteam = false

    deferrals.defer()
    
    -- mandatory wait!
    Wait(0)    
    
    deferrals.update("Hello. Your identification is being checked.")

    for _, v in pairs(identifiers) do        
        if string.sub(v, 1, string.len("steam:")) == "steam:" then
            steamid = v
            hasSteam = true
        elseif string.sub(v, 1, string.len("discord:")) == "discord:" then
            discord = v
            hasDiscord = true
        end

        if string.sub(v, 1, string.len("license:")) == "license:" then
            license = v
            allowed = true
        elseif string.sub(v, 1, string.len("xbl:")) == "xbl:" then
            xbl  = v
            allowed = true
        elseif string.sub(v, 1, string.len("ip:")) == "ip:" then
            ip = v
            allowed = true
        elseif string.sub(v, 1, string.len("live:")) == "live:" then
            liveid = v
            allowed = true
        end
        printIds = printIds .. "[" .. v .. "]"
    end
    print(printIds)
    
    -- mandatory wait!
    Wait(0)
    
    if allowed and (hasSteam or hasDiscord) then
        deferrals.done()
    else
        deferrals.done("Sorry, you need to have a valid discord or steam identification before you can join.")
        CancelEvent()
        setKickReason("Sorry, you need to have a valid discord or steam identification before you can join.")
    end    
end

AddEventHandler("playerConnecting", OnPlayerConnecting)