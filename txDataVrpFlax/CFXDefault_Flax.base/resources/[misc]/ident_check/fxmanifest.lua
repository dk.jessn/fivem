-- Resource Metadata
fx_version "adamant"
game 'gta5'

author 'dk.jessn@dumlerp'
description 'Display login ids and ensure connections with a license'
version '1.0.0'

client_scripts {
    'client.lua'
}

server_script {
    'server.lua'
}
