fx_version 'adamant'
games {"gta5"}

ui_page "client/html/index.html"

client_script 'client/main.lua'
server_script 'server/main.lua'

files {
    'client/html/index.html',
    'client/html/sounds/*.ogg'
}
