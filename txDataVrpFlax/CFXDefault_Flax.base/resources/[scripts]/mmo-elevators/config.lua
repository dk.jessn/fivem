Config = {}

Config.Circle = {
    levitating = false,
    color = { -- RGBA
        r = 240,
        g = 240,
        b = 240,
        a = 100
    }
}

-- Note: Organizing the elevators is only for the UI presentation, but it does not affect the functionality of the script.
Config.elevators = { 
    {
        id = "akselevator",
        name = "Lounch",
        floor = 3,
        coords = vector3(-1096.3397, -850.3279, 23.0386),
        heading = 30.70
    },  
    {
        id = "akselevator",
        name = "Lobby",
        floor = 2,
        coords = vector3(-1096.2360, -850.3672, 19.0012),
        heading = 30.70
    },      
    {
        id = "akselevator",
        name = "AKS rum",
        floor = 1,
        coords = vector3(-1089.7231, -848.1085, -46.2487),
        heading = 30.70,
        permission = "pd.key"
    },  

    {
        id = "vestpd2",
        name = "Helikopter",
        floor = 4,
        coords = vector3(-1096.3984, -850.1003, 38.2377),
        heading = 30.70,
        permission = "pd.key"
    },  
    {
        id = "vestpd2",
        name = "Kontor 2",
        floor = 3,
        coords = vector3(-1096.3872, -850.1051, 34.3607),
        heading = 30.70,
        permission = "pd.key"
    },  
    {
        id = "vestpd2",
        name = "Kontor 1",
        floor = 2,
        coords = vector3(-1096.3547, -849.9285, 30.7571),
        heading = 30.70,
        permission = "pd.key"
    },  
    {
        id = "vestpd2",
        name = "Træning",
        floor = 1,
        coords = vector3(-1096.2329, -850.1794, 26.8276),
        heading = 30.70,
        permission = "pd.key"
    },    


    {
        id = "vestpd1",
        name = "3. etage",
        floor = 5,
        coords = vector3(-1066.0684, -833.4935, 27.0365),
        heading = 30.70,
        permission = "pd.key"
    },
    {
        id = "vestpd1",
        name = "Lobby",
        floor = 4,
        coords = vector3(-1066.0735, -833.7689, 19.0357),
        heading = 30.70,
        permission = "pd.key"
    },
    {
        id = "vestpd1",
        name = "Omklædning",
        floor = 3,
        coords = vector3(-1066.1847, -833.3791, 14.8828),
        heading = 30.70,
        permission = "pd.key"
    },
    {
        id = "vestpd1",
        name = "-2. etage",
        floor = 2,
        coords = vector3(-1065.9810, -833.7147, 11.0372),
        heading = 30.70,
        permission = "pd.key"
    },
    {
        id = "vestpd1",
        name = "Celler",
        floor = 1,
        coords = vector3(-1065.9923, -833.7114, 5.4744),
        heading = 30.70,
        permission = "pd.key"
    },                
        -- Del Perro Heights --
    {
        id = "delperroheights",
        floor = 3,
        coords = vector3(-1452.1, -540.7, 74.05),
        heading = 30.70
    },
    {
        id = "delperroheights",
        floor = 2,
        coords = vector3(-1500, -525.9, 69.55),
        heading = 31.4
    }, 
    {
        id = "delperroheights",
        floor = 0,
        coords = vector3(-1447.7, -537.45, 34.75),
        heading = 213.70
    },
        -- Alta Sreet --
    {
        id = "3altastreet",
        floor = 1,
        coords = vector3(-273.2, -967.3, 77.2),
        heading = 247.30
    },
    {
        id = "3altastreet",
        name = "The Majesty Floor", -- optional
        floor = 2,
        coords = vector3(-269.90, -941, 92.5),
        heading = 64.70
    },  
    {
        id = "3altastreet",
        floor = 0,
        coords = vector3(-268.9, -962.35, 31.2),
        heading = 296.90
    }, 
}
