local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")

RegisterServerEvent('hasPermission')
AddEventHandler('hasPermission', function()    
    local user_id = vRP.getUserId({source})
    for _, elevator in pairs(Config.elevators) do   
        if elevator.name == nil or elevator.name == "" then
            do break end
        end
        local hasPermission = true
        if elevator.permission ~= nil and user_id ~= nil then    
            hasPermission = vRP.hasPermission({user_id, elevator.permission})
        end        
        ElevatorPermissions[elevator.name] = hasPermission                                           
    end
    TriggerClientEvent("hasPermission:return",source,ElevatorPermissions)
end)
