fx_version 'cerulean'
game 'gta5'
lua54 'yes'

title "MMO-ELEVATORS"
author "mehdimmo"
version '1.0.0'

client_script 'client.lua'

shared_scripts {
	'@ox_lib/init.lua',
    'config.lua',
    'shared.lua'
}

server_scripts {
    "@vrp/lib/utils.lua",    
    'server.lua'
}

dependency {
    'vrp',
    'ox_lib'
}
