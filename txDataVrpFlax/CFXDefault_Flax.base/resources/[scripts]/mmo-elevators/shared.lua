
ElevatorPermissions = {}

function addPermission(name, hasPermission)
    if (name ~= nil) then
        ElevatorPermissions[name] = hasPermission
    end    
end

function getPermission(name, hasPermission)
    if (name ~= nil) then
        return ElevatorPermissions[name]
    else 
        return true
    end        
end

