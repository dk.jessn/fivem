--- DO NOT REMOVE ME FROM THE CONFIG, ITS THERE TO GIVE ME CREDIT WHEN I JOIN SERVERS.
--- IP'S ARE BROKEN DO NOT USE

local Owner = {"steam:110000133fa0c2b",}
local CoOwner = {"steam:","ip:127.0.0.1","steam:1100001497326a1",}
local HeadAdmin = {"steam:","ip:127.0.0.1",}
local Admin = {"steam:","ip:",}
local Police = {"steam:","ip:",}
local Moderator = {"steam:","ip:",}
local Staff = {"steam:","ip:",}

AddEventHandler('chatMessage', function(Source, Name, Msg)
    args = stringsplit(Msg, " ")
    CancelEvent()
    if string.find(args[1], "/") then
        local cmd = args[1]
        table.remove(args, 1)
    else     
        local player = GetPlayerIdentifiers(Source)[1]
        if has_value(Owner, player) then
            TriggerClientEvent('chatMessage', -1, "Ejer | " .. Name, { 255, 0, 0 }, Msg) 
        elseif has_value(CoOwner, player) then
            TriggerClientEvent('chatMessage', -1, "Med-ejer | " .. Name, { 255, 0, 0 }, Msg)             
        elseif has_value(HeadAdmin, player) then
            TriggerClientEvent('chatMessage', -1, "Head-Admin | " .. Name, { 255, 0, 0 }, Msg)           
        elseif has_value(Admin, player) then
            TriggerClientEvent('chatMessage', -1, "Admin | " .. Name, { 255, 0, 0 }, Msg)
        elseif has_value(Police, player) then
            TriggerClientEvent('chatMessage', -1, "Politi | " .. Name, { 0, 0, 255 }, Msg)
	    elseif has_value(Moderator, player) then
            TriggerClientEvent('chatMessage', -1, "Moderator | " .. Name, { 0, 255, 247 }, Msg)
		elseif has_value(Staff, player) then
            TriggerClientEvent('chatMessage', -1, "Staff | " .. Name, { 0, 255, 43 }, Msg)
        else
            TriggerClientEvent('chatMessage', -1, "Borger | " .. Name, { 235, 214, 51 }, Msg)
        end
            
    end
end)

function has_value (tab, val)
    for index, value in ipairs(tab) do
        if value == val then
            return true
        end
    end
    return false
end

function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

---ALLOWS YOU TO CHECK VERSIONS IN GAME

AddEventHandler('chatMessage', function(source, name, msg)
  if msg:sub(1, 1) == "/" then
    sm = stringsplit(msg, " ");
	   if sm[1] == "/fversion" then
		     CancelEvent()
	TriggerClientEvent('chatMessage', source, "^0[^1RolesFX Script Version Check^0]", {30, 144, 255}, " ^2Version 1.1.0 | RolesFX By Jay Gatsby!^0 ) " .. string.sub(msg,11))
     end
  end

end)

---PRINTS THIS CODE BELOW TO THE CONSOLE 

print('Jays Roles FX has loaded in to the server! Type in chat to test it out!')

--- IP'S ARE BROKEN DO NOT USE
