--[[-----------------
 	Doors Control By XanderWP from Ukraine with <3
 ------------------------]]--

local doors = {}
local LockHotkey = {0,103} -- E

RegisterNetEvent('vrpdoorsystem:load')
AddEventHandler('vrpdoorsystem:load', function(list)
  doors = list
end)


RegisterNetEvent('vrpdoorsystem:statusSend')
AddEventHandler('vrpdoorsystem:statusSend', function(i, status)
  if doors[i] ~= nil then
    doors[i].locked = status
  end
end)

function DrawText3Ds(x,y,z, text)
  local onScreen,_x,_y=World3dToScreen2d(x,y,z)
  local px,py,pz=table.unpack(GetGameplayCamCoords())

  SetTextScale(0.32, 0.32)
  SetTextFont(4)
  SetTextProportional(1)
  SetTextColour(255, 255, 255, 255)
  SetTextEntry("STRING")
  SetTextCentre(1)
  AddTextComponentString(text)
  DrawText(_x,_y)
  local factor = (string.len(text)) / 370
  DrawRect(_x,_y+0.0125, 0.015+ factor, 0.03, 0, 0, 0, 80)
end

function searchIdDoor()
  local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
  for k,v in pairs(doors) do
    if GetDistanceBetweenCoords(x,y,z,v.coords[1],v.coords[2],v.coords[3],true) <= 2.0 then
      return k
    end
  end
  return 0
end

function toggleClosestDoor()
  local x,y,z = table.unpack(GetEntityCoords(PlayerPedId(), true))
  for k,v in pairs(doors) do
      if v.close ~= nil then
          local door1 = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), v.coords[1], v.coords[2], v.coords[3], true )
          local lradius = (v.range ~= nil and v.range or 2.0)
          if door1 < lradius then
              if door1 < GetDistanceBetweenCoords(x,y,z,doors[v.close].coords[1], doors[v.close].coords[2], doors[v.close].coords[3],true) then
                  TriggerServerEvent("vrpdoorsystem:status", k, not v.locked)
              else
                  TriggerServerEvent("vrpdoorsystem:status", v.close, not doors[v.close].locked)
              end
              break
          end
      else
          local lradius = (v.range ~= nil and v.range or 2.0)
          if GetDistanceBetweenCoords(x,y,z,v.coords[1], v.coords[2], v.coords[3],true) <= lradius then
              TriggerServerEvent("vrpdoorsystem:status", k, not v.locked)
          end
      end
  end
end

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)
    local id = searchIdDoor()
    if id ~= 0 then
      local door = GetClosestObjectOfType(doors[id].coords[1],doors[id].coords[2],doors[id].coords[3], 1.0, doors[id].hash, false, false, false)
      if doors[id].locked then
        DrawText3Ds(doors[id].coords[1], doors[id].coords[2], doors[id].coords[3], "Tryk ~g~E~w~ for at låse op")
        local locked, heading = GetStateOfClosestDoorOfType(doors[id].hash, doors[id].coords[1], doors[id].coords[2], doors[id].coords[3], doors[id].locked, 0)
        if heading > -0.01 and heading < 0.01 then
          FreezeEntityPosition(door, doors[id].locked)
        end        
      else
        DrawText3Ds(doors[id].coords[1], doors[id].coords[2], doors[id].coords[3], "Tryk ~r~E~w~ for at låse")
        FreezeEntityPosition(door, doors[id].locked)
      end      
      if IsControlJustPressed(table.unpack(LockHotkey)) then
        TriggerServerEvent("vrpdoorsystem:open", id)
        -- toggleClosestDoor()
      end
    end
  end
end)

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(10)
    local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
    for k,v in pairs(doors) do
      if GetDistanceBetweenCoords(x,y,z,v.coords[1],v.coords[2],v.coords[3],true) <= 10 then
          local door = GetClosestObjectOfType(v.coords[1],v.coords[2],v.coords[3], 1.0, v.hash, false, false, false)
          if door ~= 0 then
            SetEntityCanBeDamaged(door, false)
            if v.locked == false then
              NetworkRequestControlOfEntity(door)
              FreezeEntityPosition(door, false)
            else
              local locked, heading = GetStateOfClosestDoorOfType(v.hash, v.coords[1],v.coords[2],v.coords[3], locked, heading)
              if heading > -0.02 and heading < 0.02 then
                NetworkRequestControlOfEntity(door)
                FreezeEntityPosition(door, true)
              end
            end
          end
      end
    end
  end
end)
