--
-- Created by IntelliJ IDEA.
-- User: Sling
-- Date: 02-02-2019
-- Time: 19:14
-- Made for CiviliansNetwork
--

local cfg = {}

cfg.doors = {
    -- Centrum PD
    [1] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={467.9358215332,-1014.4254150391,26.386516571045}},
    [2] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={469.21606445313,-1014.3658447266,26.386516571045}},
    [3] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1033001619,coords={464.19165039063,-1003.5250244141,24.914739608765}},
    [4] = {locked = false, key = "key_lspd", permission="pd.key", name = "Politistation",hash=320433149,coords={434.77639770508,-982.43109130859,30.709680557251}},
    [5] = {locked = false, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1215222675,coords={434.7995300293,-981.20196533203,30.689680099487}},
    [6] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={461.87542724609,-993.60382080078,24.914846420288}},
    [7] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={461.84725952148,-998.62127685547,24.914863586426}},
    [8] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={461.88555908203,-1002.2982788086,24.91485786438}},
    [9] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={463.54235839844,-992.73272705078,24.914863586426}},
    [10] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation", hash = 1557126584, coords={450.27975463867,-986.43347167969,30.689682006836}},
    [11] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation", hash = 3261965677,coords={453.09655761719,-982.54162597656,30.689683914185}},
    [12] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation", hash=-1320876379, coords={447.2998046875,-980.09771728516,30.68967628479}},
    [13] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation", hash=185711165, coords={444.06491088867,-989.43817138672,30.689668655396}},
    [14] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation", hash=185711165, coords={445.28582763672,-989.43853759766,30.689668655396}},
    [15] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-340230128,coords={464.33865356445,-984.03485107422,43.69785690307}},
    [16] = {locked = true, key = "key_lspd", permission="pd.key", name = "Centrum PD", hash=-1603817716, coords={488.91033935547,-1017.6137695313,28.212596893311}},
    [17] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1765048490,coords={1855.0665283203,3683.5229492188,34.266490936279}},
    [18] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=507213820,coords={463.34674072266,-1011.3004150391,32.986675262451}}, 
    [19] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3261965677,coords={445.33279418945,-999.01007080078,30.724411010742}}, 
    [20] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3261965677,coords={446.52096557617,-999.09460449219,30.723945617676}}, 
    [21] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={463.88232421875,-983.96795654297,35.931072235107}},  
    [22] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3691943625,coords={429.20999145508,-995.51507568359,35.736888885498}}, 
    [23] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3691943625,coords={429.14102172852,-994.650390625,35.736850738525}}, 
    [24] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={445.53564453125,-988.83660888672,35.931098937988}}, 
    [25] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={449.80892944336,-983.22839355469,35.931083679199}}, 
    [26] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={449.68936157227,-992.82409667969,35.931083679199}}, 
    [27] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={452.12292480469,-980.28179931641,35.931087493896}}, 
    [28] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={461.21807861328,-980.208984375,35.931102752686}}, 
    [29] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={463.76354980469,-990.61730957031,35.931091308594}}, 
    [30] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={459.90264892578,-994.6142578125,35.931095123291}}, 
    [31] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={459.89678955078,-997.45263671875,35.931076049805}}, 
    [32] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=2974090917,coords={462.77459716797,-1000.9973144531,35.931076049805}}, 
    [33] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={468.48785400391,-997.95989990234,35.931083679199}}, 
    [34] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={470.95901489258,-999.47393798828,35.931083679199}}, 
    [35] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={475.3948059082,-992.03149414063,35.931083679199}}, 
    [36] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={484.85565185547,-991.90216064453,35.931076049805}}, 
    [37] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={477.88269042969,-1001.2205200195,35.931087493896}}, 
    [38] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={477.99612426758,-1010.6357421875,35.931087493896}}, 
    [39] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=2271212864,coords={446.46520996094,-997.06567382813,30.689329147339}}, 
    [40] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=2271212864,coords={445.27633666992,-997.16870117188,30.689336776733}}, 
    [41] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3751469904,coords={446.09588623047,-987.23675537109,26.674175262451}}, 
    [42] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3751469904,coords={446.21270751953,-986.00842285156,26.674171447754}}, 
    [43] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={443.24508666992,-988.32775878906,26.674167633057}}, 
    [44] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={441.99865722656,-985.83148193359,26.674167633057}}, 
    [45] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3751469904,coords={451.43774414063,-983.88220214844,26.674167633057}}, 
    [46] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3751469904,coords={452.71365356445,-983.88452148438,26.674167633057}}, 
    [47] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3751469904,coords={451.9563293457,-979.60681152344,26.668561935425}}, 
    [48] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3751469904,coords={444.90356445313,-979.40466308594,26.668563842773}}, 
    [49] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3751469904,coords={439.18212890625,-979.53308105469,26.668563842773}}, 
    [50] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={471.30429077148,-985.45574951172,24.914695739746}},
    [51] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=91564889,coords={475.59259033203,-985.99523925781,24.914695739746}},
    [52] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={468.37942504883,-977.97039794922,24.914691925049}},
    [53] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={468.37942504883,-977.97039794922,24.914691925049}},
    [54] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=4163671155,coords={463.61096191406,-981.3505859375,24.914691925049}},
    [55] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3261965677,coords={467.77423095703,-1003.4700317383,24.914710998535}}, 
    [56] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3261965677,coords={472.2414855957,-1003.6258544922,24.914710998535}}, 
    [57] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3261965677,coords={476.36901855469,-1003.5585327148,24.914710998535}},
    [58] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3261965677,coords={480.68664550781,-1003.5628051758,24.914710998535}},
    [59] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3261965677,coords={480.81872558594,-996.40118408203,24.914714813232}},
    [60] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3261965677,coords={476.54922485352,-996.49310302734,24.914710998535}},
    [61] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3261965677,coords={472.16049194336,-996.47381591797,24.914710998535}},
    [62] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3261965677,coords={467.82318115234,-996.41979980469,24.914710998535}},
    [63] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3751469904,coords={470.08618164063,-1009.859375,26.386312484741}},
    [64] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3751469904,coords={470.12448120117,-1011.1139526367,26.386312484741}},
    [65] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation", hash = 1557126584, coords={1842.78125,3690.9018554688,34.286655426025}},
    [66] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation", hash = 1557126584, coords={1848.6605224609,3690.7421875,34.286640167236}},
    [67] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation", hash = 1557126584, coords={1851.2239990234,3693.9606933594,34.286640167236}},
    [68] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation", hash = 1557126584, coords={1854.1685791016,3693.97265625,34.286647796631}},
    [69] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation", hash = 1557126584, coords={1856.5999755859,3689.8645019531,34.286659240723}},
    [70] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation", hash = 1557126584, coords={1850.8371582031,3682.7854003906,34.286643981934}},
    [71] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation", hash = 1557126584, coords={1845.5241699219,3688.9594726563,34.286609649658}},
    [72] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={1846.0446777344,3685.1311035156,34.286636352539}},
    [73] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={1848.3852539063,3681.1594238281,34.286636352539}},
    [74] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=2271212864,coords={-450.33877563477,6015.9897460938,31.716361999512}},
    [75] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=2271212864,coords={-450.33877563477,6015.9897460938,31.716361999512}},
    [76] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3775898501,coords={-453.78839111328,6011.3051757813,31.716331481934}},
    [77] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=452874391,coords={-451.48696899414,6006.58984375,31.840093612671}},
    [78] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3613901090,coords={-449.07745361328,6008.0834960938,31.716337203979}},
    [79] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=245182344,coords={-448.2126159668,6007.1684570313,31.716354370117}},
    [80] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=964838196,coords={-441.4548034668,6012.2744140625,31.71635055542}},
    [81] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=964838196,coords={-442.34408569336,6011.3979492188,31.71635055542}},
    [82] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3342610948,coords={-438.13052368164,6007.8037109375,31.716327667236}},
    [83] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3342610948,coords={-441.5553894043,6004.5219726563,31.716331481934}},
    [84] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=452874391,coords={-446.76275634766,6001.8974609375,31.716184616089}},
    [85] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3283274690,coords={-437.13568115234,5992.3920898438,31.716176986694}},
    [86] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=3138946425,coords={-440.83819580078,5989.44921875,31.716176986694}},
    [87] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-432.73403930664,5992.7749023438,31.716173171997}},
    [88] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-428.56289672852,5997.140625,31.716175079346}},
    [89] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-431.58096313477,6000.2651367188,31.716171264648}},
    [90] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=2793810241,coords={-444.01232910156,6016.5668945313,31.716339111328}},
    [91] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=2793810241,coords={-443.05529785156,6015.6323242188,31.716341018677}},
    
    -- Retssal
    [92] = {locked = true, key = "key_lspd", permission="retsag.key", name = "Retssal",hash=110411286,coords={238.95112609863,-420.45617675781,-118.46504974365}},
    [93] = {locked = true, key = "key_lspd", permission="retsag.key", name = "Retssal",hash=110411286,coords={238.51235961914,-421.61920166016,-118.46504974365}},
    -- HELLS ANGELS
    [94] = {locked = true, key = "key_hell", permission="hell.doors", name = "HellAngels",hash=190770132,coords={981.60498046875,-102.7546081543,74.845146179199}},
    [95] = {locked = true, key = "key_hell", permission="hell.doors", name = "HellAngels",hash=-2125423493,coords={959.97784423828,-140.08976745605,74.486106872559}, range = 5.0}, 
    --Cartel huset
    [96] = {locked = true, key = "key_cartel", permission="cartel.doors", name = "Cartel",hash=-1032171637,coords={1390.5715332031,1131.7316894531,114.33407592773}, pairs = 85},
    [97] = {locked = true, key = "key_cartel", permission="cartel.doors", name = "Cartel",hash=-52575179,coords={1390.5875244141,1132.6441650391,114.33406829834}, pairs = 84},
    [98] = {locked = true, key = "key_cartel", permission="cartel.doors", name = "Cartel",hash=-1032171637,coords={1399.9057617188,1128.2419433594,114.33451843262}, pairs = 87},
    [99] = {locked = true, key = "key_cartel", permission="cartel.doors", name = "Cartel",hash=-52575179,coords={1401.1546630859,1128.2502441406,114.33450317383}, pairs = 86},
    [100] = {locked = true, key = "key_cartel", permission="cartel.doors", name = "Cartel",hash=262671971,coords={1395.7215576172,1141.2496337891,114.64886474609}, pairs = 89},
    [101] = {locked = true, key = "key_cartel", permission="cartel.doors", name = "Cartel",hash=1504256620,coords={1395.7955322266,1142.2257080078,114.6512298584}, pairs = 88},
    -- SOA Coke salg
    [102] = {locked = true, key = "key_soa_salg", permission="soa.doors", name = "SonsOfAnarchy",hash=-1230442770,coords={241.80499267578,360.41781616211,105.61915588379}},
    -- Bilforhandleren
    [103] = {locked = false, key = "key_bilforhandler", permission="bilforhandler.doors", name = "Bilforhandler",hash=2059227086,coords={-38.636848449707,-1108.3608398438,26.468864440918}, pairs = 92},
    [104] = {locked = false, key = "key_bilforhandler", permission="bilforhandler.doors", name = "Bilforhandler",hash=1417577297,coords={-37.818347930908,-1108.7191162109,26.468883514404}, pairs = 91},
    [105] = {locked = false, key = "key_bilforhandler", permission="bilforhandler.doors", name = "Bilforhandler",hash=-2051651622,coords={-34.122013092041,-1108.2292480469,26.422351837158}},
    [106] = {locked = false, key = "key_bilforhandler", permission="bilforhandler.doors", name = "Bilforhandler",hash=-2051651622,coords={-31.903076171875,-1102.4223632813,26.422357559204}},
    [107] = {locked = false, key = "key_bilforhandler", permission="bilforhandler.doors", name = "Bilforhandler",hash=2059227086,coords={-60.018104553223,-1093.4749755859,26.67373085022}, pairs = 96},
    [108] = {locked = false, key = "key_bilforhandler", permission="bilforhandler.doors", name = "Bilforhandler",hash=1417577297,coords={-60.425937652588,-1094.2043457031,26.673414230347}, pairs = 95},    
    [109] = {locked = false, key = "key_bilforhandler", permission="bilforhandler.doors", name = "Bilforhandler",hash=1286535678,coords={-12.702404022217,-1088.6901855469,26.672050476074}, range = 10.0}, 
    [110] = {locked = false, key = "key_bilforhandler", permission="bilforhandler.doors", name = "Bilforhandler",objName="prop_com_ls_door_01",coords={29.32,-1086.49,27.58}, range = 10.0},
    [111] = {locked = true, key = "key_dvs", permission="dvs.doors", name = "DVS Kontor",hash=220394186,coords={-74.445838928223,-821.83734130859,243.38580322266}},
    [112] = {locked = true, key = "key_dvs", permission="dvs.doors", name = "DVS Kontor",hash=220394186,coords={-75.087455749512,-821.66241455078,243.38578796387}},
    [113] = {locked = true, key = "key_dvs", permission="dvs.doors", name = "DVS Kontor",hash=4206024936,coords={-77.76016998291,-814.38348388672,243.38580322266}},
    [114] = {locked = true, key = "key_dvs", permission="dvs.doors", name = "DVS Kontor",hash=4206024936,coords={-77.463996887207,-807.87969970703,243.38589477539}},
    -- The Lost
    [115] = {locked = true, key = "key_lost", permission="lost.doors", name = "LostMC", hash=993120320, coords={-564.64434814453,276.7370300293,83.136421203613}}, -- 565,1712 Y:276,6259 Z:83,28626 -- 3270313599
    [116] = {locked = true, key = "key_lost", permission="lost.doors", name = "LostMC", hash=993120320, coords={-561.76635742188,293.54418945313,87.628456115723}}, -- X:-561,2866 Y:293,5044 Z:87,77851
    [117] = {locked = true, key = "key_lost", permission="lost.doors", name = "LostMC", hash=-626684119, coords={-569.72430419922,293.27856445313,79.176689147949}}, -- X:-569,7971 Y:293,7701 Z:79,3264
    [118] = {locked = true, key = "key_lost", permission="lost.doors", name = "LostMC", hash=1289778077, coords={-568.40148925781,280.98806762695,82.976020812988}},
    [119] = {locked = true, key = "key_lost", permission="lost.doors", name = "LostMC", hash=4044124512, coords={-547.41107177734,302.52340698242,83.020469665527}},
    -- Ultimates Hus
    [120] = {locked = true, key = "key_ultimate", permission="ultimate.doors", name = "Jake Longs Hus", hash=2169543803, coords={-1471.1927490234,68.518379211426,53.305465698242}},
    [121] = {locked = true, key = "key_ultimate", permission="ultimate.doors", name = "Jake Longs Hus", hash=2435496056, coords={-1461.7110595703,65.899688720703,52.933738708496}},
    [122] = {locked = true, key = "key_ultimate", permission="ultimate.doors", name = "Jake Longs Hus", hash=2169543803, coords={-1613.5920410156,78.079833984375,61.576831817627}},
    [123] = {locked = true, key = "key_ultimate", permission="ultimate.doors", name = "Jake Longs Hus", hash=2435496056, coords={-1579.0186767578,152.81965637207,58.67639541626}},
    [124] = {locked = true, key = "key_ultimate", permission="ultimate.doors", name = "Jake Longs Hus", hash=2435496056, coords={-1434.5480957031,235.57548522949,59.960693359375}},
    [125] = {locked = true, key = "key_ultimate", permission="ultimate.doors", name = "Jake Longs Hus", hash=3636940819, coords={-1500.8984375,103.72638702393,55.673488616943}},
    [126] = {locked = true, key = "key_ultimate", permission="ultimate.doors", name = "Jake Longs Hus", hash=3636940819, coords={-1503.2437744141,105.40780639648,55.657718658447}},
    [127] = {locked = true, key = "key_ultimate", permission="ultimate.doors", name = "Jake Longs Hus", hash=4030239080, coords={-1517.8248291016,107.93916320801,51.847808837891}},
    [128] = {locked = true, key = "key_ultimate", permission="ultimate.doors", name = "Jake Longs Hus", hash=4030239080, coords={-1579.0186767578,152.81965637207,58.67639541626}},
    -- Hospital
    [129] = {locked = false, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=661758796, coords={299.83953857422,-584.14093017578,43.283962249756}},
    [130] = {locked = false, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=3807058540, coords={299.39419555664,-585.40423583984,43.283920288086}},
    [131] = {locked = false, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=3860183810, coords={303.61099243164,-581.63165283203,43.283996582031}},
    [132] = {locked = false, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=2594055320, coords={304.41796875,-581.91040039063,43.283973693848}},
    [133] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={304.23986816406,-571.65380859375,43.284019470215}},
    [134] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={307.78228759766,-569.72808837891,43.284019470215}},
    [135] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=3860183810, coords={312.67034912109,-571.55096435547,43.284019470215}},
    [136] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=2594055320, coords={313.66152954102,-571.94775390625,43.283981323242}},
    [137] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=3860183810, coords={318.3498840332,-573.72723388672,43.284023284912}},
    [138] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=2594055320, coords={319.51168823242,-574.07012939453,43.283977508545}},
    [139] = {locked = false, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=3860183810, coords={317.76519775391,-578.94396972656,43.284015655518}},
    [140] = {locked = false, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=2594055320, coords={316.77359008789,-578.67926025391,43.283981323242}},
    [141] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=3860183810, coords={326.31323242188,-578.75958251953,43.283996582031}},
    [142] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=2594055320, coords={326.01681518555,-579.65777587891,43.283985137939}},
    [143] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={337.01110839844,-580.38861083984,43.283992767334}},
    [144] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={341.56381225586,-582.00628662109,43.283996582031}},
    [145] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={347.51892089844,-584.16912841797,43.283996582031}},
    [146] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={339.60565185547,-587.02368164063,43.283996582031}},
    [147] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={337.51666259766,-592.93511962891,43.283996582031}},
    [148] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={329.2844543457,-585.92510986328,43.284023284912}},
    [149] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={328.47564697266,-588.16320800781,43.284019470215}},
    [150] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=3860183810, coords={349.16143798828,-586.92004394531,43.284019470215}},
    [151] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=2594055320, coords={348.80184936523,-587.96539306641,43.283981323242}},
    [152] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={357.30841064453,-580.40832519531,43.284008026123}},
    [153] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={355.84494018555,-584.32263183594,43.284030914307}},
    [154] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={351.94232177734,-594.97515869141,43.283988952637}},
    [155] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={350.58721923828,-598.62506103516,43.283996582031}},
    [156] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={345.79275512695,-596.38800048828,43.283996582031}},
    [157] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={347.19128417969,-592.69812011719,43.283996582031}},
    [158] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={359.42608642578,-594.13470458984,43.284030914307}},
    [159] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={361.37548828125,-589.240234375,43.284030914307}},
    [160] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=2594055320, coords={325.80084228516,-589.87786865234,43.283973693848}},
    [161] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=3860183810, coords={325.02819824219,-589.57958984375,43.284000396729}},
    [162] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={313.09921264648,-596.38787841797,43.284553527832}},
    [163] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={308.38586425781,-597.478515625,43.284023284912}},
    [164] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={303.59295654297,-597.23376464844,43.284019470215}},
    [165] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=854291622, coords={298.6526184082,-595.54827880859,43.284019470215}},
    [166] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=3860183810, coords={327.85021972656,-593.55859375,43.284019470215}},
    [167] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=2594055320, coords={327.59701538086,-594.49951171875,43.283977508545}},
    [168] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=3860183810, coords={324.07040405273,-575.73614501953,43.284008026123}},
    [169] = {locked = true, key = "key_ems", permission="ems.doors", name = "Hospitalet", hash=2594055320, coords={324.81591796875,-576.01751708984,43.28396987915}},
--DBE
    [170] = {locked = false, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=4204511029, coords={105.8420715332,-745.60382080078,45.754745483398}}, 
    [171] = {locked = false, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=2777093385, coords={106.16389465332,-743.87658691406,45.754749298096}},

    [172] = {locked = true, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=2473190209, coords={127.57320404053,-763.78625488281,242.15196228027}},
    [173] = {locked = true, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=2473190209, coords={120.55218505859,-763.67803955078,242.15211486816}},
    [174] = {locked = true, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=2473190209, coords={114.51921844482,-757.98931884766,242.15214538574}},
    [175] = {locked = true, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=2473190209, coords={113.52660369873,-738.97680664063,242.15214538574}},
    [176] = {locked = true, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=2473190209, coords={119.06577301025,-734.00885009766,242.15203857422}},
    [177] = {locked = true, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=2473190209, coords={121.28192901611,-756.91882324219,242.15197753906}},
    [178] = {locked = true, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=3441107298, coords={133.10946655273,-767.98022460938,242.15205383301}},
    [179] = {locked = true, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=2473190209, coords={138.87535095215,-768.09936523438,242.15209960938}},
    [180] = {locked = true, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=2473190209, coords={143.38024902344,-759.6865234375,242.15202331543}},
    [181] = {locked = true, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=2473190209, coords={146.61227416992,-755.50994873047,242.1519317627}},
    [182] = {locked = true, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=2473190209, coords={151.24235534668,-742.86413574219,242.15196228027}},
    [183] = {locked = true, key = "key_dbe", permission="dbe.doors", name = "DBE Kontor", hash=2473190209, coords={152.74691772461,-738.71826171875,242.15209960938}},
-- Centrum PD New    
    [190] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-96679321,coords={441.2115, -986.4204, 30.7884, 168.9653}},
    [191] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1547307588,coords={442.2729, -998.6068, 30.7117, 172.0833}},
    [192] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1547307588,coords={441.4207, -998.6351, 30.7468, 13.9670}},
    [193] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1406685646,coords={441.3665, -977.6204, 30.7903, 354.5339}},
    [194] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1547307588,coords={456.7430, -972.3035, 30.7098, 0.4670}},
    [195] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1547307588,coords={457.5423, -972.2081, 30.7098, 352.9981}}, 
    [196] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-692649124,coords={468.9179, -1014.3990, 26.4257, 166.6047}}, 
    [197] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-692649124,coords={468.0934, -1014.4858, 26.4258, 170.5558}}, 
    [198] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-53345114,coords={485.0065, -1007.7579, 26.3208, 329.5623}}, 
    [199] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-53345114,coords={486.0115, -1012.1722, 26.3230, 184.5350}}, 
    [200] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-53345114,coords={483.0242, -1012.1392, 26.3230, 200.8929}}, 
    [201] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-53345114,coords={479.9967, -1012.2075, 26.3230, 162.0193}}, 
    [202] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-53345114,coords={477.0901, -1012.1169, 26.3230, 173.4613}}, 
    [203] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=1830360419,coords={464.2496, -996.6384, 26.3740, 102.9892}}, 
    [204] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=1830360419,coords={464.2874, -975.4967, 26.3740, 85.4260}}, 
    [205] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-692649124,coords={464.3297, -983.7376, 43.7748, 120.3720}}, 
    [206] = {locked = false, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-692649124,coords={434.7421, -982.4803, 30.7131, 243.2545}},    -- Entrance 
    [207] = {locked = false, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-692649124,coords={434.7697, -981.3712, 30.6994, 85.5595}},     -- Entrance
    [208] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=2130672747,coords={452.3621, -1000.9644, 25.7395, 191.0445}}, 
    [209] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=2130672747,coords={431.2214, -1000.8242, 25.7194, 170.7388}},     
    [210] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-53345114,coords={476.6270, -1008.1992, 26.3228, 275.4937}},     
    [211] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-53345114,coords={481.7402, -1004.0884, 26.3230, 345.6055}},     
    [212] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1406685646,coords={482.5865, -996.3951, 26.3740, 240.9550}},         
    [213] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1406685646,coords={482.6811, -993.1375, 26.3740, 276.6886}},       
    [214] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1406685646,coords={482.6752, -988.4858, 26.3740, 266.3006}},        
    [215] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1406685646,coords={482.6309, -984.8824, 26.3740, 269.8889}},  

    -- Abe Gang
    [216] = {locked = true, key = "key_abegang", permission="keys.abegang", name = "Abe Gang",hash=1901183774,coords={-2667.4509, 1326.2323, 147.4450, 269.4357}},  -- Entrance  
    [217] = {locked = true, key = "key_abegang", permission="keys.abegang", name = "Abe Gang",hash=-147325430,coords={-2667.1399, 1330.0664, 147.4451, 1.1941}},    -- Basement

    -- Black Cobra Gang
    [218] = {locked = true, key = "key_gsfgang", permission="gsf.doors", name = "Black Cobra Gang",hash=1436076651,coords={85.9496, -1960.1167, 21.1214, 235.6394}},  -- Entrance  
    
    -- Vest PD --    
    [219] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-147325430,coords={-1090.0785, -841.0325, 37.6337, 127.4941}},
    [220] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1090.8795, -809.0972, 19.3771, 218.7027}},      -- Entrance
    [221] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1091.8005, -809.9629, 19.3742, 267.1645}},   -- Entrance
    [222] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1092.9650, -810.7925, 19.3781, 40.1181}},    -- Entrance
    [223] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1093.9554, -811.6374, 19.3607, 54.4061}},    -- Entrance
    [224] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1255368438,coords={-1091.6947, -818.2161, 19.0362, 30.4618}},
    [225] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1255368438,coords={-1092.4326, -818.7469, 19.0358, 31.9130}},   
    [226] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1061.9563, -827.3143, 19.4327, 311.8012}}, -- Entrance
    [227] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1061.3441, -828.2323, 19.4281, 313.9246}}, -- Entrance
    [228] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1255368438,coords={-1099.0587, -835.1198, 19.0016, 117.7526}},
    [229] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1255368438,coords={-1098.1863, -836.1139, 19.0015, 303.7668}},
    [230] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-147325430,coords={-1091.2358, -841.4535, 18.3573, 127.5977}},  -- firedoor stairs
    [231] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-147325430,coords={-1077.2653, -830.2531, 19.0358, 38.6642}},  -- firedoor stairs    
    [232] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1106.1278, -845.8694, 19.3169, 124.8314}},      -- Entrance
    [233] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1106.8947, -844.9495, 19.3164, 127.6632}},      -- Entrance
    [234] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1107.9760, -843.4927, 19.3169, 117.9557}},      -- Entrance
    [235] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1108.6317, -842.6805, 19.3169, 125.1295}},      -- Entrance
    [236] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1112.1208, -825.4421, 19.3188, 34.1372}},      -- Entrance
    [237] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1111.2490, -824.7509, 19.3194, 43.0054}},      -- Entrance
    [238] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1111.6899, -848.4031, 13.4834, 307.2205}},
    [239] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-350181704,coords={-1112.4108, -847.4963, 13.4829, 306.5375}}, 
    [240] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1108.2897, -842.1786, 13.6849, 36.6298}},
    [241] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1089.9514, -848.0768, 4.8841, 120.9986}},   
    [242] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1090.6564, -847.1085, 4.8841, 134.0495}},
    [243] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1057.3065, -839.5970, 5.1095, 218.5337}},  --back
    [244] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1058.2727, -840.3690, 5.1101, 219.3065}},  --back
    [245] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1079.6044, -855.7355, 5.2051, 218.6694}},  --back
    [246] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-1072.9780, -827.1519, 5.4797, 32.0237}},
    [247] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-1087.1442, -829.2750, 5.4797, 33.8205}},
    [248] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-1086.2050, -827.3461, 5.4798, 318.1484}}, --Celle
    [249] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-1089.3273, -829.6307, 5.4798, 127.6921}}, --Celle
    [250] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-1088.6084, -824.1984, 5.4800, 311.2827}}, --Celle
    [251] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-1091.7332, -826.4348, 5.4799, 134.8834}}, --Celle
    [252] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-1090.9834, -820.9742, 5.4799, 323.9292}}, --Celle
    [253] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-1094.1293, -823.2368, 5.4799, 125.5858}}, --Celle
    [254] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-1096.5653, -820.1365, 5.4799, 124.2463}}, --Celle
    -- Luft --
    [255] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=871712474,coords={-1125.7906, -2758.7063, 21.3613, 59.6368}}, --Celle
    [256] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=871712474,coords={-1122.1115, -2760.7659, 21.3614, 247.1755}}, --Celle
    [257] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=871712474,coords={-1123.6451, -2763.5457, 21.3613, 244.5946}}, --Celle
    [258] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=871712474,coords={-1127.3217, -2761.4048, 21.3613, 54.4779}}, --Celle
     --Vest Pd 
    [259] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1092.8623, -817.8835, 5.4800, 304.3959}},
    [260] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1093.5106, -816.8975, 5.4799, 297.8840}},
    [261] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1090.5215, -812.5584, 5.4799, 44.8801}},
    [262] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1085.4645, -812.0220, 5.4799, 311.3449}},
    [263] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1086.0897, -811.0839, 5.4799, 304.3830}},
    [264] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1091.0322, -835.4539, 5.4797, 311.2835}},
    [265] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1091.8735, -834.5115, 5.4799, 294.2919}},
    [266] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1074.2251, -822.1105, 5.4799, 221.7354}},
    [267] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1075.1193, -822.8425, 5.4799, 221.4234}},
    [268] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1081.7770, -816.9150, 5.4799, 140.0461}},
    [269] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-2023754432,coords={-1078.1207, -814.1050, 5.4800, 305.7374}},
    [270] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-147325430,coords={-1091.1016, -841.4943, 22.3571, 310.0856}},  -- firedoor stairs
    --Syd Pd 
    [271] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1047370197,coords={368.8236, -1608.0381, 29.2921}},  
    [272] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=1738519111,coords={369.6398, -1606.8304, 29.2921}},  
    [273] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=471928866,coords={361.9085, -1584.6577, 29.2920}},  
    [274] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1360054856,coords={360.9553, -1585.6740, 29.2920}},  
    [275] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=471928866,coords={354.3719, -1593.4972, 29.2920}}, 
    [276] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1360054856,coords={353.6768, -1594.3192, 29.2920}}, 
    --Nord Pd 
    [277] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-333.4807, -274.5722, 32.0670}}, --Celle
    [278] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-329.8503, -277.2784, 32.0670}}, --Celle
    [279] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-326.3136, -279.8681, 32.0670}}, --Celle 
    [280] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-322.5769, -282.5310, 32.0670}}, --Celle  
    [281] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-334.5982, -272.0009, 32.0670}},
    [282] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-340.1264, -270.2928, 32.0670}},

    [283] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1796068320,coords={-311.9215, -278.2791, 31.7199, 55.0166}}, -- V dør
    [284] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=560252123,coords={-311.3014, -277.5981, 31.7171, 59.7024}}, -- H dør
    [285] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1796068320,coords={-327.8061, -254.5548, 34.3950, 59.5155}}, -- V dør
    [286] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=560252123,coords={-327.4099, -253.8834, 34.3946, 48.6736}}, -- H dør
    [287] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1796068320,coords={-367.0059, -241.2967, 36.1246, 240.5877}}, -- V dør
    [288] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=560252123,coords={-367.4744, -241.8954, 36.1274, 229.9954}}, -- H dør   
    [289] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=631614199,coords={-314.6750, -276.0514, 32.0556}},    
    [290] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-131296141,coords={-328.4652, -265.9218, 28.0672, 331.5141}}, -- P dør
    --syd pd --
    [291] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1842288246,coords={361.9336, -1611.0970, 29.2921, 318.0177}}, --Celle
    [292] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1842288246,coords={359.6876, -1609.1047, 29.2921, 327.4225}}, --Celle
    [293] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1842288246,coords={357.3303, -1607.2856, 29.2921, 314.3300}}, --Celle 
    [294] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1842288246,coords={355.3136, -1605.5111, 29.2921, 321.2504}}, --Celle  
    [295] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1842288246,coords={353.1543, -1603.6425, 29.2921, 322.5094}}, --Celle 
    [296] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1842288246,coords={350.1894, -1601.3243, 29.2921, 312.9531}},
    [297] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1685865813,coords={356.3448, -1596.7599, 29.2921, 233.6537}},
    [298] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-667323357,coords={371.7254, -1586.4893, 29.2920, 235.6411}},
    [299] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-667323357,coords={370.9303, -1587.3958, 29.2920, 233.1460}},
    [300] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-667323357,coords={358.0773, -1592.5048, 29.2920, 144.2985}},
    [301] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-667323357,coords={357.1443, -1591.5961, 29.2920, 143.7002}},
    [302] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-1685865813,coords={364.3165, -1595.3586, 29.2920, 225.0755}},
    --Nord PD --
    [303] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-543497392,coords={-352.7427, -260.3711, 36.0664, 232.9564}}, 
    [304] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-543497392,coords={-352.0049, -259.3589, 36.0664, 235.5948}}, 
    [305] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-543497392,coords={-345.1841, -250.0481, 36.0664, 238.0904}},  
    [306] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-543497392,coords={-344.5011, -249.1021, 36.0664, 243.5116}}, 
    [307] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-543497392,coords={-348.2797, -254.0054, 39.7460, 239.8385}},
    [308] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-543497392,coords={-349.1040, -255.0248, 39.7460, 238.7627}},
    [309] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-219887954,coords={-360.6049, -239.0235, 36.0799, 227.8421}},
    [310] = {locked = true, key = "key_lspd", permission="pd.key", name = "Politistation",hash=-131296141,coords={-362.4281, -252.4212, 32.9371, 235.7602}}, --P dør

}
return cfg