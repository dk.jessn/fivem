Thank you for downloading. I do ask you do not put a price on this or claim it as yours.

Ariel Set
8 textures for the top and skirt

Original creator: SMSims
https://www.patreon.com/SMSims/posts

Converted and rigged by AdoredRose

Installation -

Single player:
Get mpclothes here: https://www.gta5-mods.com/misc/mpclothes-addon-clothing-slots
1. Open OpenIV and enable Edit mode
2. Drag and drop files to mpclothes DLC pack, changing numbers in their file name if they are overlapping with files that you've already gotten
Get mpclothes here: https://www.gta5-mods.com/misc/mpclothes-addon-clothing-slots

FiveM:
Get Durty Cloth Tool here:
https://github.com/DurtyFree/durty-cloth-tool
1. Open Durty Cloth Tool and create a new project/use your existing one
2. Put the files into any folder
3. Import files from that folder

Join the Discord for more free converted clothing! All my converts are free and always will be
https://discord.gg/adoredrose

Adored Rose Clothing Licensing Agreement

1. Complimentary Access:
- Adored Rose's clothing offers a selection of high-quality attire, completely free of charge, to enhance your single-player or FiveM server experience.

2. No Commercial Activities:
- You are expressly prohibited from engaging in any form of commercial activity related to the clothing provided by Adored Rose. This includes, but is not limited to, selling, leasing, or otherwise profiting from our clothing items.

3. Respect for Original Creators:
- The clothing designs in our collection have been ported from The Sims 4 by talented creators. To maintain their creative integrity, you must adhere to the original licensing agreements associated with these designs. Respect the terms set by the creators.

4. Non-Transferable Rights:
- The rights granted under this license are non-transferable. You, as the licensee, are the sole party permitted to use Adored Rose's clothing within the bounds of this agreement.

By downloading and utilizing Adored Rose's clothing, you acknowledge and agree to these terms and conditions. Failure to comply may result in the revocation of your rights to use our attire.

I appreciates your commitment to upholding these principles, allowing me to continue sharing my fashion passion with the world. Thank you for your understanding and support.


