resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

description "F1 Car Pack"

files {
    'Ferrari/data/*.meta',
    'Mercedes/data/*.meta',
    'Redbull/data/*.meta',
}

--Ferrari
data_file 'HANDLING_FILE' 'Ferrari/data/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'Ferrari/data/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'Ferrari/data/carvariations.meta'
data_file 'VEHICLE_LAYOUTS_FILE''Ferrari/data/vehiclelayouts.meta'

--Mercedes
data_file 'HANDLING_FILE' 'Mercedes/data/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'Mercedes/data/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'Mercedes/data/carvariations.meta'
data_file 'VEHICLE_LAYOUTS_FILE''Mercedes/data/vehiclelayouts.meta'

--Redbull
data_file 'HANDLING_FILE' 'Redbull/data/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'Redbull/data/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'Redbull/data/carvariations.meta'
data_file 'VEHICLE_LAYOUTS_FILE''Redbull/data/vehiclelayouts.meta'