--
-- FIREAC (https://github.com/AmirrezaJaberi/FIREAC)
-- Copyright 2022-2023 by Amirreza Jaberi (https://github.com/AmirrezaJaberi)
-- Licensed under the GNU Affero General Public License v3.0
--

FIREAC                          = {}
--                                           * 𝗧𝗜𝗣 𝟭 *
--                               Type of Punishment : BAN | KICK | WARN
--
--                                           * 𝗧𝗜𝗣 𝟮 *
--                                           Screenshot
--                            For enable screenshot download this resources
--                     (https://github.com/jaimeadf/discord-screenshot/releases)
--                                 Add this resource to your server

--【 𝗩𝗲𝗿𝘀𝗶𝗼𝗻 𝗖𝗵𝗲𝗰𝗸 】--
FIREAC.Version                  = "6.2.2"

--【 𝗦𝗲𝗿𝘃𝗲𝗿 𝗦𝗲𝘁𝘁𝗶𝗻𝗴𝘀 】--
FIREAC.ServerConfig             = {
    Name = "DumleRP",
    Port = "30120",
}

--【 𝗟𝗼𝗴 𝗼𝗳 𝗗𝗶𝘀𝗰𝗼𝗿𝗱 】--
FIREAC.Log                      = {
    Ban        = "",
    Error      = "",
    Connect    = "",
    Disconnect = "",
    Exoplosion = "",
}

--【 𝗖𝗵𝗮𝘁 𝗦𝗲𝘁𝘁𝗶𝗻𝗴𝘀 】--
FIREAC.ChatSettings             = {
    Enable      = true,
    PrivateWarn = true,
}

--【 𝗦𝗰𝗿𝗲𝗲𝗻𝗦𝗵𝗼𝘁 】--
FIREAC.ScreenShot               = {
    Enable  = true,
    Format  = "PNG",
    Quality = 1,
    Log     = ""
}

--【 𝗖𝗼𝗻𝗻𝗲𝗰𝘁𝗶𝗼𝗻 𝗦𝗲𝘁𝘁𝗶𝗻𝗴𝘀 】--
FIREAC.Connection               = {
    AntiBlackListName = true,
    AntiVPN           = true,
    HideIP            = true,
}

--【 𝗠𝗲𝘀𝘀𝗮𝗴𝗲 】--
FIREAC.Message                  = {
    Kick = "You Are ⚡️ Kicked From DumleRP. Protection By FIREAC® - Don't try to cheat on this Server",
    Ban  = "You Are ⛔️ Banned From DumleRP. Please make ticket in our Discord Server ",
}

--【 𝗔𝗱𝗺𝗶𝗻 𝗠𝗲𝗻𝘂 】--
FIREAC.AdminMenu                = {
    Enable         = true,
    Key            = "INSERT",
    MenuPunishment = "KICK",
}

--【 𝗔𝗻𝘁𝗶 𝗧𝗿𝗮𝗰𝗸 𝗣𝗹𝗮𝘆𝗲𝗿 】--
FIREAC.AntiTrackPlayer          = true
FIREAC.MaxTrack                 = 10
FIREAC.TrackPunishment          = "WARN"

--【 𝗔𝗻𝘁𝗶 𝗛𝗲𝗮𝗹𝘁𝗵 𝗛𝗮𝗰𝗸 】--
FIREAC.AntiHealthHack           = true 
FIREAC.MaxHealth                = 201
FIREAC.HealthPunishment         = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗔𝗿𝗺𝗼𝗿 𝗛𝗮𝗰𝗸 】--
FIREAC.AntiArmorHack            = true 
FIREAC.MaxArmor                 = 100
FIREAC.ArmorPunishment          = "WARN"

--【 𝗔𝗻𝘁𝗶 𝗧𝗮𝘀𝗸𝘀 】--
FIREAC.AntiBlacklistTasks       = false
FIREAC.TasksPunishment          = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗣𝗹𝗮𝘆 𝗔𝗻𝗶𝗺𝘀 】--
FIREAC.AntiBlacklistAnims       = false
FIREAC.AnimsPunishment          = "KICK"

--【 𝗦𝗮𝗳𝗲 𝗣𝗹𝗮𝘆𝗲𝗿𝘀 】--
FIREAC.SafePlayers              = false
FIREAC.AntiInfinityAmmo         = true

--【 𝗔𝗻𝘁𝗶 𝗦𝗽𝗲𝗰𝘁𝗮𝘁𝗲 】--
FIREAC.AntiSpectate             = true
FIREAC.SpactatePunishment       = "WARN"

--【 𝗔𝗻𝘁𝗶 𝗪𝗲𝗮𝗽𝗼𝗻 】--
FIREAC.AntiBlackListWeapon      = true
FIREAC.AntiAddWeapon            = false
FIREAC.AntiRemoveWeapon         = false
FIREAC.AntiWeaponsExplosive     = true
FIREAC.WeaponPunishment         = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗚𝗼𝗱𝗠𝗼𝗱𝗲 】--
FIREAC.AntiGodMode              = true
FIREAC.GodPunishment            = "WARN"

--【 𝗔𝗻𝘁𝗶 𝗜𝗻𝘃𝗶𝘀𝗶𝗯𝗹𝗲 】--
FIREAC.AntiInvisible            = false 
FIREAC.InvisiblePunishment      = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗖𝗵𝗮𝗻𝗴𝗲 𝗦𝗽𝗲𝗲𝗱 】--
FIREAC.AntiChangeSpeed          = false 
FIREAC.SpeedPunishment          = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗙𝗿𝗲𝗲 𝗖𝗮𝗺 】--
FIREAC.AntiFreeCam              = false
FIREAC.CamPunishment            = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗥𝗮𝗶𝗻𝗯𝗼𝘄 𝗩𝗲𝗵𝗶𝗰𝗹𝗲 】--
FIREAC.AntiRainbowVehicle       = true
FIREAC.RainbowPunishment        = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗣𝗹𝗮𝘁𝗲 】--
FIREAC.AntiPlateChanger         = true
FIREAC.AntiBlackListPlate       = true
FIREAC.PlatePunishment          = "WARN"

--【 𝗔𝗻𝘁𝗶 𝗩𝗶𝘀𝗶𝗼𝗻 】--
FIREAC.AntiNightVision          = false
FIREAC.AntiThermalVision        = false
FIREAC.VisionPunishment         = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗦𝘂𝗽𝗲𝗿 𝗝𝘂𝗺𝗽 】--
FIREAC.AntiSuperJump            = true
FIREAC.JumpPunishment           = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗧𝗲𝗹𝗲𝗽𝗼𝗿𝘁 】--
FIREAC.AntiTeleport             = false
FIREAC.MaxFootDistance          = 200
FIREAC.MaxVehicleDistance       = 600
FIREAC.TeleportPunishment       = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗡𝗼𝗰𝗹𝗶𝗽 】--
FIREAC.AntiNoclip               = false
FIREAC.NoclipPunishment         = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗣𝗲𝗱 𝗖𝗵𝗮𝗻𝗴𝗲𝗿 】--
FIREAC.AntiPedChanger           = true
FIREAC.PedChangePunishment      = "WARN"

--【 𝗔𝗻𝘁𝗶 𝗜𝗻𝗳𝗶𝗻𝗶𝘁𝗲 𝗦𝘁𝗮𝗺𝗶𝗻𝗮 】--
FIREAC.AntiInfiniteStamina      = false
FIREAC.InfinitePunishment       = "WARN"

--【 𝗔𝗻𝘁𝗶 𝗥𝗮𝗴𝗱𝗼𝗹𝗹 】--
FIREAC.AntiRagdoll              = false
FIREAC.RagdollPunishment        = "WARN"

--【 𝗔𝗻𝘁𝗶 𝗠𝗲𝗻𝘆𝗼𝗼 】--
FIREAC.AntiMenyoo               = true
FIREAC.MenyooPunishment         = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗔𝗶𝗺 𝗔𝘀𝘀𝗶𝘀𝘁 】--
FIREAC.AntiAimAssist            = true
FIREAC.AimAssistPunishment      = "WARN"

--【 𝗔𝗻𝘁𝗶 𝗥𝗲𝘀𝗼𝘂𝗿𝗰𝗲 】--
FIREAC.AntiResourceStopper      = false
FIREAC.AntiResourceStarter      = false
FIREAC.AntiResourceRestarter    = false
FIREAC.ResourcePunishment       = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗖𝗵𝗮𝗻𝗴𝗲 𝗙𝗹𝗮𝗴 】--
FIREAC.AntiTinyPed              = false
FIREAC.PedFlagPunishment        = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗦𝘂𝗶𝗰𝗶𝗱𝗲 】--
FIREAC.AntiSuicide              = true
FIREAC.SuicidePunishment        = "WARN"

--【 𝗔𝗻𝘁𝗶 𝗖𝗼𝗹𝗹𝗲𝗰𝘁𝗲𝗱 𝗣𝗶𝗰𝗸𝘂𝗽 】--
FIREAC.AntiPickupCollect        = true
FIREAC.PickupPunishment         = "WARN"

--【 𝗔𝗻𝘁𝗶 𝗖𝗵𝗮𝘁 】--
FIREAC.AntiSpamChat             = true
FIREAC.MaxMessage               = 25
FIREAC.CoolDownSec              = 3
FIREAC.ChatPunishment           = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗖𝗼𝗺𝗺𝗮𝗻𝗱 】--
FIREAC.AntiBlackListCommands    = true
FIREAC.CMDPunishment            = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗖𝗵𝗮𝗻𝗴𝗲 𝗗𝗮𝗺𝗮𝗴𝗲 】--
FIREAC.AntiWeaponDamageChanger  = true
FIREAC.AntiVehicleDamageChanger = true
FIREAC.DamagePunishment         = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗪𝗼𝗿𝗱 】--
FIREAC.AntiBlackListWord        = false 
FIREAC.WordPunishment           = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗕𝗿𝗶𝗻𝗴 𝗔𝗹𝗹 】--
FIREAC.AntiBringAll             = true
FIREAC.BringAllPunishment       = "WARN"

--【 𝗔𝗻𝘁𝗶 𝗧𝗿𝗶𝗴𝗴𝗲𝗿 】--
FIREAC.AntiBlackListTrigger     = true
FIREAC.AntiSpamTrigger          = true
FIREAC.TriggerPunishment        = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗖𝗹𝗲𝗮𝗿 𝗣𝗲𝗱 𝗧𝗮𝘀𝗸𝘀 】--
FIREAC.AntiClearPedTasks        = true
FIREAC.MaxClearPedTasks         = 5
FIREAC.CPTPunishment            = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗧𝗮𝘇𝗲 𝗣𝗹𝗮𝘆𝗲𝗿𝘀 】--
FIREAC.AntiTazePlayers          = true
FIREAC.MaxTazeSpam              = 3
FIREAC.TazePunishment           = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗜𝗻𝗷𝗲𝗰𝘁 】--
FIREAC.AntiInject               = true
FIREAC.InjectPunishment         = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗘𝘅𝗽𝗹𝗼𝘀𝗶𝗼𝗻 】--
FIREAC.AntiBlackListExplosion   = false 
FIREAC.AntiExplosionSpam        = true 
FIREAC.MaxExplosion             = 20
FIREAC.ExplosionSpamPunishment  = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗘𝗻𝘁𝗶𝘁𝘆 𝗦𝗽𝗮𝘄𝗻𝗲𝗿 】--
FIREAC.AntiBlackListObject      = false
FIREAC.AntiBlackListPed         = false
FIREAC.AntiBlackListBuilding    = false
FIREAC.AntiBlackListVehicle     = false
FIREAC.EntityPunishment         = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗘𝗻𝘁𝗶𝘁𝘆 𝗦𝗽𝗮𝗺𝗲𝗿 】--
FIREAC.AntiSpawnNPC             = false

FIREAC.AntiSpamVehicle          = false
FIREAC.MaxVehicle               = 10

FIREAC.AntiSpamPed              = true
FIREAC.MaxPed                   = 2

FIREAC.AntiSpamObject           = true
FIREAC.MaxObject                = 10

FIREAC.SpamPunishment           = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗖𝗵𝗮𝗻𝗴𝗲 𝗣𝗲𝗿𝗺 】--
FIREAC.AntiChangePerm           = true
FIREAC.PermPunishment           = "KICK"

--【 𝗔𝗻𝘁𝗶 𝗣𝗹𝗮𝘆 𝗦𝗼𝘂𝗻𝗱 】--
FIREAC.AntiPlaySound            = true
FIREAC.SoundPunishment          = "WARN"
