[< Back](README.md)

# FiveM Server Installation Instructions

## Trin 0 - Buy Patreon membership

_This step is optional._

Buy [Argentum membership](https://www.patreon.com/join/fivem) to allow and enable usage of custom clothing such as your own uniforms.

## Step 1 - Install database

Download and install MySQL Community Edition.

_Remember the password for the database!_

```
https://dev.mysql.com/downloads/mysql/
```

## Step 2 - Download Modding Framework

Download modding framework (this repository) to your computer either by using Git as shown below or by clicking **Download** at [GitLab](https://gitlab.com/dk.jessn/fivem).

_It is recommended to use the FlaxHosting files._

```
git clone https://gitlab.com/dk.jessn/fivem.git
```

## Step 3 - Install FiveM Server artefacts

Download the _latest_ or recommended version and install/extract the binaries to the `server` folder.

```
https://runtime.fivem.net/artifacts/fivem/build_server_windows/master/
```

## Step 4 - Create database

Connect to the database instance using MySQL Workbench and creating a database using the command `CREATE SCHEMA fivem DEFAULT CHARACTER SET utf8 COLLATE utf8_danish_ci;`.

### Database only for ESX

Create all database tables using the script **database.sql**, which is placed in the folder `txDataEsx\ESXLegacy_010DCE.base`.

### Database only for DEVO

Create all database tables using the script **database.sql**, which is placed in the folder `txDataVrpDevo\CFXDefault_Devo.base`.

## Step 5 - Configure server

Create the file **configurations.cfg** with the context below in one of the foldering folders, depending on which server you will use, and change as a minimum the following `sv_licenseKey`, `sv_hostname`, `sv_projectName`, `sv_projectDesc`, `mysql_connection_string` in the file content.  

- For Dunko VRP <br>
txDataVrpDunko\CFXDefault_E9235B.base
- For ESX <br>
txDataEsx\ESXLegacy_010DCE.base
- For DEVO <br>
txDataVrpDevo\CFXDefault_Devo.base

```configurations.cfg
set mysql_connection_string "mysql://USERNAME:PASSWORD@localhost/fivem?multipleStatements=true"
sets Discord ""
sets YouTube ""
sets tags ""

sv_hostname ""
sets sv_projectName ""
sets sv_projectDesc ""

sv_licenseKey ""
set steam_webApiKey ""

sv_maxclients 32
```

### Configufation file only for VRP Dunko and DEVO

Change the file **config.json** in `resources\ghmattimysql` with the content below and change as a minimum the following `user`, `password` og `database` in the content.  

```config.json
{
    "user": "",
    "password": "",
    "host": "localhost",
    "port": 3306,
    "database": "fivem"
}
```

## Step 6 - Starting the server 

Start the server from one of of the following folders, depending on which server you will use, with the command below. 

- For Dunko VRP  <br>
txDataVrpDunko\CFXDefault_E9235B.base
- For ESX <br>
txDataEsx\ESXLegacy_010DCE.base
- For DEVO <br>
txDataVrpDevo\CFXDefault_Devo.base

```
start ../../server/FXServer
```

Create a file **start.bat** with the content below in the folder.

```start.bat
@echo off
start ../../server/FXServer
```

Start the server using batch file respectively.

## Step 7 - Re-brand the server

_This step is optional._

You have to change the names and pictures in the files below if you want to re-brand the server.

- txDataVrpFlax\CFXDefault_Flax.base/logo.png
- txDataVrpFlax\CFXDefault_Flax.base/server.cfg
- txDataVrpFlax\CFXDefault_Flax.base/server_global_settings.cfg
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/gcphone/html/static/config/config.json
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/gcphone/html/static/img/background
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/dn-disclaimer/ui/img/bg-img.png
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/dn-info/img/header.png
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/dn-loadingscreen/Loading.mp3
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/dn-loadingscreen/Loading.ogg
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/dn-loadingscreen/loading.png
- txDataVrpFlax\CFXDefault_Flax.base/resources/[vrp]/vrp/modules/gui.lua `line 151`
- txDataVrpFlax\CFXDefault_Flax.base/resources/[vrp]/vrp/gui/design.css `line 173`
- txDataVrpFlax\CFXDefault_Flax.base/resources/[Misc]/logo/ui/img/logo.png
- txDataVrpFlax\CFXDefault_Flax.base/resources/[vrp]/vrp/cfg/lang/da.lua `line 6`
- txDataVrpFlax\CFXDefault_Flax.base/resources/[dn-scripts]/dn-ui/bare.lua `line 6`
