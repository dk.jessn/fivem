# Retningslinjer for bidrag til kode

## Introduktion

Denne korte guide giver instruktioner til de udviklere, der planlægger at bidrage med kode til et rollespilsserverprojekt.

## Indhold til et bidrag

Bidrag skal indeholde:

* Kode (naturligvis): Dette er "kernen" i bidraget. Sandsynligvis selvforklarende.
* Tests: Uanset om dit bidrag er ny funktionalitet eller en fejlrettelse, skal det altid være testet. 
* Udviklerdokumentation (kommentarer): Giv inline-dokumentation med passende detaljer. 
* Brugerdokumentation (hvis nødvendigt): Hvis du implementerer en ny funktion eller udvider funktionaliteten, skal der være en passende dokumentation, der giver brugerne mulighed for rent faktisk at bruge funktionaliteten.

## Kode

Gør hvad du skal gøre. Bare sørg for at læse følgende (dem der er relevante):

* Udviklingsretningslinjer
* GUI-udviklingsvejledning
* GUI Look and Feel Style Guide
* Konnektorudviklingsvejledning

Du bør lave dit bidrag i en feature branch baseret på den seneste tilstand af master branch. Hvis dit bidrag er baseret på ældre branches (f.eks. vedligeholdelsesbranches), vil du højst sandsynligt blive bedt om at rebase dit bidrag til en master branch. Master branch er den eneste langsigtede "levende" branch for udvikling. Support branches er "blindgyde"-branches. De vil aldrig blive merged tilbage til master. Disse branches er der kun til støtteformål (backport). Derfor giver bidrag baseret på disse branches ikke rigtig mening.

Sørg altid for, at din kode ikke bryder nogen eksisterende funktionalitet.

## Kredit

Git vedligeholder commit-metadataene for den oprindelige commit. Og dette er, hvad der vil blive registreret i historiesporet for hovedmidtpunkts-lageret. Derfor vil den oprindelige bidragyder blive registreret i hver commit. Bortset fra dette kan bidragyderne frit tilføje deres navne til det passende sted i filoverskriften, hvis de føler, at deres bidrag er stort nok til at retfærdiggøre det.

## Bidragsmekanismer

Den foretrukne måde at give et bidrag på er at følge pull request-proceduren på git. Denne metode er ret enkel, hurtig og ligetil. For alle bidrag, store som små, er det påkrævet at oprette en ny feature branch. Når du er færdig med bidraget, skal du blot oprette et nyt pull request på git. Kodeejerne vil blive underrettet, koden vil blive gennemgået, og der gives feedback. Du får chancen for at forbedre bidraget, og endelig vil vi enten acceptere eller afvise bidraget. Alt det kan gøres på git.

