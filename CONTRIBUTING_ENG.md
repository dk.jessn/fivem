# Code Contribution Guidelines

## Introduction

This short guide provides instructions for the developers that plan to contribute code this a roleplay server project.

## Content of Contribution

Contribution should contain:

* Code (obviously): This is the "core" of the contribution. Probably self-explanatory.
* Tests: Whether your contribution is new functionality or a bugfix, it must always be appropriately tested. 
* Developer documentation (comments): Provide inline documentation at an appropriate granularity. 
* User documentation (if needed): If you implement new feature or extend  functionality there needs to be an appropriate documentation that allows users to actually use the functionality.

## Code

Do what you have to do. Just make sure to read through following (those that are applicable):

* Development Guidelines
* GUI Development Guide
* GUI Look and Feel Style Guide
* Connector Development Guide

You should apply your contribution to a feature branch based the most recent state of the master branch. If your contribution is based on older branches (e.g. maintenance branches) you will be very likely asked to rebase your contribution to a master branch. The master branch is the only long-term "living" branch for development. Support branches are "dead end" branches. They will never get merged back into the master. Those branches are there only for support (backport) purposes. Therefore contribution based on those branches does not really make sense.

Always make sure that your code does not break any existing functionality. 

## Credit

Git maintains the commit meta-data of the original commit. And this is what will be recorded in the history trail of main midPoint repository. Therefore the original contributor will be recorded in each commit. Apart from this the contributors are free to add their names to the appropriate place in the file header if they feel their contribution is big enough to justify it.

## Contribution Mechanics

Preferred way to make a contribution is to follow the pull request procedure on git. This method is quite simple, fast and straightforward. For all contributions, big and small, it is required to create a new feature branch. When you are done with the contribution simply create new pull request on git. The code owners will be notified, the code will be reviewed, and feedback is provided. You will have the chance to improve the contribution and finally we will either accept or refuse the contribution. All of that can be done on git. 

