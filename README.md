# FiveM Server 

## Intro

Dette er en mindre fejlbehæftet og mere stabil version af det eksisterende VRP rammeværk. Det er ment til at være et opensource projekt for at genoplive det gamle og ellers velfungerende VRP rammeværk.

//

This is a less error-prone and more stable version of the existing VRP framework. The purpose is to keep it as an opensource project to revive the old and well-functioning VRP framework.

## Installationsvejledning // Installation Instructions

### Vælg sprog // Choose language

* [Dansk](README_DAN.md)
* [English](README_ENG.md)

## Anerkendelser // Acknowledgement

Tak til [Dunko vRP](https://github.com/vRP-Official-Developers/dunko_vrp), [Devo Network](https://github.com/servercfg/DevoNetwork), [Flax Hosting](https://github.com/KianFrostholm/FlaxHosting-Filer) og [Guilherme Caulada](https://github.com/guicaulada/FiveM-Scripts) for det initielle til denne pakke.

//

Thanks to [Dunko vRP](https://github.com/vRP-Official-Developers/dunko_vrp), [Devo Network](https://github.com/servercfg/DevoNetwork), [Flax Hosting](https://github.com/KianFrostholm/FlaxHosting-Filer) and [Guilherme Caulada](https://github.com/guicaulada/FiveM-Scripts) for the initial base used for this package.

## Projekt status // Project status

Der ydes ikke support eller hjælp til brug af denne installationspakke.

Du skal være velkommen til at [oprette et udestående](https://gitlab.com/dk.jessn/fivem/-/issues), hvis du finder en fejl, men der er ingen garanti for svartid.

Venligst brug [CFX Forum](https://forum.cfx.re/) for hurtigere hjælp til løsning af problemer.

//

No support nor help is provided with this installation package.

Feel free to [create an issue](https://gitlab.com/dk.jessn/fivem/-/issues), if you find a bug, but no response time is promised.

Otherwise, please use [CFX Forum](https://forum.cfx.re/) for quick help to solve the problems.

## Bidrag til kode // Contributions to code

Hvis du er interesseret i at bidrage til projektet, er du velkommen!

Projektet er gratis og open source software, men at bidrage handler ikke kun om at skrive kode. En bidragyder er enhver person, der arbejder for at forbedre og tilføje værdi til projektet og dets brugere. Dette kan betyde alt fra at rette tastefejl i dokumentationen, til at yde support, til at teste nye vækststrategier, til at implementere nye funktioner og alt derimellem.

[Retningslinjer](CONTRIBUTING_DAN.md)

//

If you're interested in contributing to the project, welcome!

The project is free and open source software, but contributing is not just about writing code. A contributor is any individual who works to improve and add value to the the project and its users. This can mean anything from fixing typos in documentation, to providing support, to testing out new growth strategies, to implementing new  features and everything in between.

[Guidelines](CONTRIBUTING_ENG.md)

## Donationer // Donations

Du er velkommen til at komme med et økonomisk bidrag til at vedligeholde og videreudvikle serveren, eller hvis du blot ønsker at købe mig en kop kaffe.

[Betalingslink](https://buy.stripe.com/00geVu85jdb03uw9AD)

//

Feel free to give an economic constribution to maintainance and development of the server, or if you just want to buy me a cup of coffee.

[Payment link](https://buy.stripe.com/00geVu85jdb03uw9AD)

## Licens // License

Denne installationspakke er licenseret under en MIT opensource licens.

[MIT Licens](LICENSE)

//

This installation package is licensed under the MIT opensource license.

[MIT License](LICENSE)

