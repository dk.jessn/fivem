SET FOREIGN_KEY_CHECKS=0;

delete from vrp_user_business where user_id > 3;
delete from vrp_user_data where user_id > 3;
delete from vrp_user_homes where user_id > 3;
delete from vrp_user_identities where user_id > 3;
delete from vrp_user_ids where user_id > 3;
delete from vrp_user_moneys where user_id > 3;
delete from vrp_user_tokens where user_id > 3;
delete from vrp_user_vehicles where user_id > 3;
delete from vrp_users where id > 3;

ALTER TABLE vrp_users AUTO_INCREMENT=100;

-- delete from vrp_user_vehicles where user_id > 3;
-- update vrp_user_data set dvalue = '[]' where user_id > 3 and dkey ='vRP:datatable';

SET FOREIGN_KEY_CHECKS=1;

