CREATE PROCEDURE sp_swap_ids (IN oldId INT, IN newId INT)
BEGIN
  DECLARE tempId INT;
  SET FOREIGN_KEY_CHECKS=0;
  
  SELECT COUNT(id) + 100 INTO tempId FROM vrp_users;

  update vrp_users set id = tempId where id = newId;
  update vrp_user_business set user_id = tempId where user_id = newId;
  update vrp_user_data set user_id = tempId where user_id = newId;
  update vrp_user_homes set user_id = tempId where user_id = newId;
  update vrp_user_identities set user_id = tempId where user_id = newId;
  update vrp_user_ids set user_id = tempId where user_id = newId;
  update vrp_user_moneys set user_id = tempId where user_id = newId;
  update vrp_user_tokens set user_id = tempId where user_id = newId;
  update vrp_user_vehicles set user_id = tempId where user_id = newId;


  update vrp_users set id = newId where id = oldId;
  update vrp_user_business set user_id = newId where user_id = oldId;
  update vrp_user_data set user_id = newId where user_id = oldId;
  update vrp_user_homes set user_id = newId where user_id = oldId;
  update vrp_user_identities set user_id = newId where user_id = oldId;
  update vrp_user_ids set user_id = newId where user_id = oldId;
  update vrp_user_moneys set user_id = newId where user_id = oldId;
  update vrp_user_tokens set user_id = newId where user_id = oldId;
  update vrp_user_vehicles set user_id = newId where user_id = oldId;


  update vrp_users set id = oldId where id = tempId;
  update vrp_user_business set user_id = oldId where user_id = tempId;
  update vrp_user_data set user_id = oldId where user_id = tempId;
  update vrp_user_homesset user_id = oldId where user_id = tempId;
  update vrp_user_identities set user_id = oldId where user_id = tempId;
  update vrp_user_ids set user_id = oldId where user_id = tempId;
  update vrp_user_moneys set user_id = oldId where user_id = tempId;
  update vrp_user_tokens set user_id = oldId where user_id = tempId;
  update vrp_user_vehicles set user_id = oldId where user_id = tempId;
  
  SET FOREIGN_KEY_CHECKS=1;
END;