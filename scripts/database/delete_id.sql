SET FOREIGN_KEY_CHECKS=0;

delete from vrp_user_business where user_id = :newId;
delete from vrp_user_data where user_id = :newId;
delete from vrp_user_homes where user_id = :newId;
delete from vrp_user_identities where user_id = :newId;
delete from vrp_user_ids where user_id = :newId;
delete from vrp_user_moneys where user_id = :newId;
delete from vrp_user_tokens where user_id = :newId;
delete from vrp_user_vehicles where user_id = :newId;
delete from vrp_users where id = :newId;

SET FOREIGN_KEY_CHECKS=1;