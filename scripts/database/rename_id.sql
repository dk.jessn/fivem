SET FOREIGN_KEY_CHECKS=0;

update vrp_users 
set id = :newId
where id = :oldId;

update vrp_user_business 
set user_id = :newId
where user_id = :oldId;

update vrp_user_data 
set user_id = :newId
where user_id = :oldId;

update vrp_user_homes
set user_id = :newId
where user_id = :oldId;

update vrp_user_identities 
set user_id = :newId
where user_id = :oldId;

update vrp_user_ids 
set user_id = :newId
where user_id = :oldId;

update vrp_user_moneys 
set user_id = :newId
where user_id = :oldId;

update vrp_user_tokens
set user_id = :newId
where user_id = :oldId;

update vrp_user_vehicles 
set user_id = :newId
where user_id = :oldId;

SET FOREIGN_KEY_CHECKS=1;